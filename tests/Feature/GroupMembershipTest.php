<?php
/**
 * Created by PhpStorm.
 * User: rotunomp
 * Date: 6/19/18
 * Time: 2:51 PM
 */

namespace Tests\Feature;

use Illuminate\Foundation\Testing\TestResponse;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Service\CredentialValidator\CredentialValidatorInterface;
use MiamiOH\RESTng\Testing\UsesAuthentication;
use Tests\TestCase;

class GroupMembershipTest extends GoogleAppsTestCase
{
    private $groupId;

    public function setUp(): void
    {
        parent::setUp();

        $this->groupId = 'testingGroup';

//        $this->showExceptions();
    }

    public function testAuthenticatedGroupMembershipResourceIsNotAuthorizedWithInvalidToken(): void
    {
        $this->willNotAuthenticateUser();

        $response = $this->getJson('/googleApps/groupMembership/v3/fakeGroupId');

        $this->assertRequestIsNotAuthorized($response);
    }

    public function testAccessIsDeniedWhenAuthorizationFails(): void
    {
        $this->willNotAuthorizeUser();

        $response = $this->getJson('/googleApps/groupMembership/v3/' . $this->groupId . '?token=fakeToken');

        $this->assertRequestIsNotAuthorized($response);
    }

    public function testAccessIsAllowedWhenAuthorizationSucceeds(): void
    {
        $response = $this->getJson('/googleApps/groupMembership/v3/' . $this->groupId . '?token=fakeToken');

        $this->assertRequestIsAuthorized($response);
    }

    public function testCanGetGroupMembership(): void
    {
        $this->groupId = 'genservok';

        $response = $this->getJson('/googleApps/groupMembership/v3/' . $this->groupId . '?token=fakeToken');
        $this->assertRouteName('googleApps.groupMembership.v3.read');

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    [
                        'memberType',
                        'memberId',
                        'directMember',
                        'role',
                    ]
                ],
            ]);
    }

    public function testCanCreateGroupMembership(): void
    {
        $this->assertGroupMembershipHasBeenCreated([]);
    }

    public function testCanDeleteGroupMembership(): void
    {
        $response = $this->deleteJson('/googleApps/groupMembership/v3?token=fakeToken&groupId=' . $this->groupId . '&memberId=' . $this->user . '&role=MEMBER');
        $this->assertRouteName('googleApps.groupMembership.v3.delete');

        $response->assertStatus(200);

    }

    public function testResponseUnauthorizedWhenNoTokenProvided(): void
    {
        $response = $this->getJson('/googleApps/groupMembership/v3/bob');
        $response->assertStatus(401);
    }

    private function assertGroupMembershipHasBeenCreated(array $membershipData = []): void
    {
        $response = $this->callCreateGroupMembership($membershipData);
//        $this->assertRouteName('googleApps.groupMembership.v3.create');

        $response->assertStatus(App::API_CREATED);
    }

    public function assertRequestIsNotAuthorized(TestResponse $response): void
    {
        $response->assertStatus(App::API_UNAUTHORIZED);
    }

    private function callCreateGroupMembership($overrides = []): TestResponse
    {
        return $this->postJson('/googleApps/groupMembership/v3?token=fakeToken&groupId=' . $this->groupId . '&memberId=' . $this->user . '&domain=' . $this->domain);
    }

    // Currently don't know a good way to test this
//    public function testCannotCreateGroupMembershipWithInvalidDataModel(): void
//    {
//        // Post without providing a groupId
//        $response = $this->postJson('/googleApps/groupMembership/v3?token=fakeToken&groupId=&memberId=' . $this->user . '&domain=' . $this->domain);
//
//        $response->assertStatus(App::API_BADREQUEST);
//    }

}
