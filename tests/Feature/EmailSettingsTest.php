<?php

namespace Tests\Feature;


use Illuminate\Foundation\Testing\TestResponse;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Testing\UsesAuthentication;
use Tests\TestCase;

class EmailSettingsTest extends GoogleAppsTestCase
{

    public function setUp(): void
    {
        parent::setUp();
    }

    public function testAuthenticatedEmailSettingsResourceIsNotAuthorizedWithInvalidToken(): void
    {
        $this->willNotAuthenticateUser();

        $response = $this->getJson('/googleApps/emailSettings/v3/bob');

        $this->assertRequestIsNotAuthorized($response);
    }

    public function testAccessIsDeniedWhenAuthorizationFails(): void
    {
        $this->willNotAuthorizeUser();

        $response = $this->getJson('/googleApps/emailSettings/v3/'.$this->user . '?token=fakeToken');

        $this->assertRequestIsNotAuthorized($response);
    }

    public function testAccessIsAllowedWhenAuthorizationSucceeds(): void
    {
        $response = $this->getJson('/googleApps/emailSettings/v3/'.$this->user . '?token=fakeToken');

        $this->assertRequestIsAuthorized($response);
    }


    public function testCanGetEmailSettings(): void
    {
        $response = $this->getJson('/googleApps/emailSettings/v3/' . $this->user . '?token=fakeToken');
        $this->assertRouteName('googleApps.emailSettings.v3.read');

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'sendas_addresses' => [
                        [
                            'default',
                            'address',
                            'sendasid',
                            'name',
                            'replyto',
                            'verified'
                        ]
                    ]
                ],
            ]);
    }

    public function testCanCreateAndDeleteEmailSettings(): void
    {
        $sendasaddress = 'adminrotunomp@gdev.miamioh.edu';
        $data = [
            'address' => $sendasaddress,
            'name' => 'OneTwo ThreeFourFiveTWO',
            'replyto' => 'admin_rotuno_m@gdev.miamioh.edu',
            'makeDefault' => 'false'
        ];

        $this->assertEmailSettingsHaveBeenCreated($data);

        $deleteResponse = $this->deleteJson('/googleApps/emailSettings/v3/' . $this->user . '/'. $sendasaddress . '?token=fakeToken');

//        $this->assertRouteName('googleApps.emailSettings.v3.delete');
        $deleteResponse->assertStatus(App::API_OK);

    }

    public function testResponseUnauthorizedWhenNoTokenProvided(): void
    {
        $response = $this->getJson('/googleApps/emailSettings/v3/bob');
        $response->assertStatus(401);
    }

    private function assertEmailSettingsHaveBeenCreated(array $settingsData = []): void
    {
        $response = $this->callCreateEmailSettings($settingsData);
        $this->assertRouteName('googleApps.emailSettings.v3.create');

        $response->assertStatus(App::API_CREATED);
    }

    public function assertRequestIsNotAuthorized(TestResponse $response): void
    {
        $response->assertStatus(App::API_UNAUTHORIZED);
    }

    private function callCreateEmailSettings($overrides = []): TestResponse
    {
        $settingsData = array_merge([
                'address' => 'adminrotunomp@gdev.miamioh.edu',
                'name' => 'OneTwo ThreeFourFiveTWO',
                'replyTo' => 'admin_rotuno_m@gdev.miamioh.edu',
                'makeDefault' => 'false'
        ], $overrides);

        return $this->postJson('/googleApps/emailSettings/v3/'. $this->user . '?token=fakeToken&domain=' . $this->domain, $settingsData);
    }

    public function testCannotCreateEmailSettingsWithInvalidDataModel(): void
    {
        $invalidDataModel = [
            [
                'default' => 'no',
                'address' => '12345@gdev.miamioh.edu',
                'sendasid' => '12345@gdev.miamioh.edu',
                'name' => 'OneTwo ThreeFourFive',
                'replyto' => '12345@gdev.miamioh.edu',
                'verified' => 'yes'
            ],
            [
                'parameter1' => 'gggggg',
                'parameter2' => 'hhhhhh',
                'parameter3' => 'iiiiii'
            ]
        ];

        $response = $this->postJson('/googleApps/emailSettings/v3/bob?token=fakeToken', $invalidDataModel);

        $response->assertStatus(App::API_BADREQUEST);
    }

}
