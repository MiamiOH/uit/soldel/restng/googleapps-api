<?php
/**
 * Created by PhpStorm.
 * User: rotunomp
 * Date: 6/19/18
 * Time: 4:02 PM
 */

namespace Tests\Feature;


use Illuminate\Foundation\Testing\TestResponse;
use MiamiOH\GoogleApps\Api\GoogleGroupSourceService;
use MiamiOH\RESTng\App;
use PHPUnit\Framework\MockObject\MockObject;

class GroupSettingsTest extends GoogleAppsTestCase
{
    private $groupId;

    public function setUp(): void
    {
        parent::setUp();

        $this->groupId = 'genservok';

//        $this->showExceptions();
    }


    public function testAuthenticatedGroupSettingsResourceIsNotAuthorizedWithInvalidToken(): void
    {
        $this->willNotAuthenticateUser();

        $response = $this->getJson('/googleApps/group/settings/v3/fakeGroupId?token=fakeToken');

        $this->assertRequestIsNotAuthorized($response);
    }

    public function testAccessIsDeniedWhenAuthorizationFails(): void
    {
        $this->willNotAuthorizeUser();

        $response = $this->getJson('/googleApps/group/settings/v3/' . $this->groupId . '?token=fakeToken');

        $this->assertRequestIsNotAuthorized($response);
    }

    public function testAccessIsAllowedWhenAuthorizationSucceeds(): void
    {
        $response = $this->getJson('/googleApps/group/settings/v3/' . $this->groupId . '?token=fakeToken');

        $this->assertRequestIsAuthorized($response);
    }

    public function testCanGetGroupSettings(): void
    {
        $this->withToken('fakeToken')
            ->withoutAcceptHeader()
            ->willAuthenticateUser();

        $response = $this->getJson('/googleApps/group/settings/v3/' . $this->groupId . '?token=fakeToken');
        $this->assertRouteName('googleApps.group.settings.v3.read');

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'allow_external_members',
                    'allow_google_communication',
                    'archive_only',
                    'allow_web_posting',
                    'custom_reply_to',
                    'default_message_deny_notification_text',
                    'description',
                    'email',
                    'include_in_global_address_list',
                    'is_archived',
                    'max_message_bytes',
                    'members_can_post_as_the_group',
                    'message_display_font',
                    'message_moderation_level',
                    'name',
                    'reply_to',
                    'send_message_deny_notification',
                    'show_in_group_directory',
                    'spam_moderation_level',
                    'who_can_contact_owner',
                    'who_can_invite',
                    'who_can_join',
                    'who_can_leave_group',
                    'who_can_post_message',
                    'who_can_view_group',
                    'who_can_view_membership',
                    'mu_population_method',
                ],
            ]);
    }

    public function testCanUpdateGroup(): void
    {
        $updateData = [
            'description' => 'Yeah'
        ];

        $response = $this->putJson('/googleApps/group/settings/v3/' . $this->groupId . '?token=fakeToken', $updateData);
        $this->assertRouteName('googleApps.group.settings.v3.update');

        $response->assertStatus(200);

    }

    public function testResponseUnauthorizedWhenNoTokenProvided(): void
    {
        $response = $this->getJson('/googleApps/group/settings/v3/bob');
        $response->assertStatus(401);
    }

    public function assertRequestIsNotAuthorized(TestResponse $response): void
    {
        $response->assertStatus(App::API_UNAUTHORIZED);
    }

}
