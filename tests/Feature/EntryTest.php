<?php
/**
 * Created by PhpStorm.
 * User: rotunomp
 * Date: 6/19/18
 * Time: 4:02 PM
 */

namespace Tests\Feature;


use Illuminate\Foundation\Testing\TestResponse;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Service\CredentialValidator\CredentialValidatorInterface;
use MiamiOH\RESTng\Testing\UsesAuthentication;
use Tests\TestCase;

class EntryTest extends GoogleAppsTestCase
{
    private $groupId;

    public function setUp(): void
    {
        parent::setUp();

        if($this->domain == 'gdev.miamioh.edu') {
            $this->groupId = 'testingGroup';
        } elseif ($this->domain == 'gtest.miamioh.edu') {
            $this->groupId = 'duit-jimmytest2';
        } elseif ($this->domain == 'miamioh.edu') {
            $this->groupId = '' . $this->domain;
        }

        $this->showExceptions();
    }


    public function testAuthenticatedEntryResourceIsNotAuthorizedWithInvalidToken(): void
    {
        $this->willNotAuthenticateUser();

        $response = $this->getJson('/googleApps/entry/v3/bob?token=fakeToken');

        $this->assertRequestIsNotAuthorized($response);
    }

    public function testAccessIsDeniedWhenAuthorizationFails(): void
    {
        $this->willNotAuthorizeUser();

        $response = $this->getJson('/googleApps/entry/v3/' . $this->user.'?token=fakeToken&domain=' . $this->domain);

        $this->assertRequestIsNotAuthorized($response);
    }

    public function testAccessIsAllowedWhenAuthorizationSucceeds(): void
    {
        $response = $this->getJson('/googleApps/entry/v3/' . $this->user.'?token=fakeToken&domain=' . $this->domain);

        $this->assertRequestIsAuthorized($response);
    }

    public function testCanGetUserEntry(): void
    {
        $response = $this->getJson('/googleApps/entry/v3/' . $this->user.'?token=fakeToken&domain=' . $this->domain);
        $this->assertRouteName('googleApps.entry.v3.read');

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'type',
                    'user',
                    'username',
                    'domain',
                    'firstName',
                    'lastName',
                    'isAdmin',
                    'ipWhiteListed',
                    'suspended',
                    'mustChangePassword',
                    'organization',
                    'nickname',
                    'groupMembership',
                    'indexed',
                ],
            ]);
    }

    public function testResponseUnauthorizedWhenNoTokenProvided(): void
    {
        $response = $this->getJson('/googleApps/entry/v3/bob');
        $response->assertStatus(401);
    }

    public function assertRequestIsNotAuthorized(TestResponse $response): void
    {
        $response->assertStatus(App::API_UNAUTHORIZED);
    }

}