<?php

namespace Tests\Feature;


use Illuminate\Foundation\Testing\TestResponse;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Testing\UsesAuthentication;
use Tests\TestCase;

class OrgUserTest extends GoogleAppsTestCase
{

    private $orgUnit;

    public function setUp(): void
    {
        parent::setUp();

        $this->orgUnit = '';

//        $this->showExceptions();
    }

    public function testAuthenticatedUserResourceIsNotAuthorizedWithInvalidToken(): void
    {
        $this->willNotAuthenticateUser();

        $response = $this->postJson('/googleApps/orgUser/v3?token=fakeToken');

        $this->assertRequestIsNotAuthorized($response);
    }

    public function testAccessIsDeniedWhenAuthorizationFails(): void
    {
        $this->willNotAuthorizeUser();

        $response = $this->callCreateOrgUser();

        $this->assertRequestIsNotAuthorized($response);
    }

    public function testAccessIsAllowedWhenAuthorizationSucceeds(): void
    {
        $response = $this->callCreateOrgUser();

        $this->assertRequestIsAuthorized($response);
    }


    public function testCanCreateOrgUser(): void
    {
        $this->assertOrgUserHasBeenCreated([]);
    }

    public function testResponseUnauthorizedWhenNoTokenProvided(): void
    {
        $response = $this->postJson('/googleApps/orgUser/v3');
        $response->assertStatus(401);
    }

    private function assertOrgUserHasBeenCreated(array $orgUserData = []): void
    {
        $response = $this->callCreateOrgUser($orgUserData);
        $this->assertRouteName('googleApps.orgUser.v3.create');

        $response->assertStatus(App::API_CREATED)
            ->assertJsonStructure([
                'data' => [
                    'username',
                    'orgUnit',
                ],
            ]);
    }

    public function assertRequestIsNotAuthorized(TestResponse $response): void
    {
        $response->assertStatus(App::API_UNAUTHORIZED);
    }

    private function callCreateOrgUser($overrides = []): TestResponse
    {
        $orgUserData = array_merge([
            'user' => $this->user,
            'orgUnit' => $this->orgUnit,
        ], $overrides);

        return $this->postJson('/googleApps/orgUser/v3?token=fakeToken', $orgUserData);
    }

    public function testCannotCreateOrgUserWithInvalidDataModel(): void
    {
        $invalidDataModel = [
            'parameter1' => 'gggggg',
            'parameter2' => 'hhhhhh',
            'parameter3' => 'iiiiii'
        ];

        $response = $this->postJson('/googleApps/orgUser/v3?token=fakeToken', $invalidDataModel);

        $response->assertStatus(App::API_BADREQUEST);
    }



}
