<?php
/**
 * Created by PhpStorm.
 * User: rotunomp
 * Date: 6/21/18
 * Time: 3:03 PM
 */

namespace Tests\Feature;

use Illuminate\Foundation\Testing\TestResponse;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Service\CredentialValidator\CredentialValidatorInterface;
use MiamiOH\RESTng\Testing\UsesAuthentication;
use Tests\TestCase;

class UserNicknamesTest extends GoogleAppsTestCase
{

    public function setUp(): void
    {
        parent::setUp();

//        $this->showExceptions();
    }

    public function testAuthenticatedUserNicknamesResourceIsNotAuthorizedWithInvalidToken(): void
    {
        $this->willNotAuthenticateUser();

        $response = $this->getJson('/googleApps/user/nicknames/v3/bob?token=fakeToken');

        $this->assertRequestIsNotAuthorized($response);
    }

    public function testAccessIsDeniedWhenAuthorizationFails(): void
    {
        $this->willNotAuthorizeUser();

        $response = $this->getJson('/googleApps/user/nicknames/v3/tepeds?token=fakeToken&domain=' . $this->domain);

        $this->assertRequestIsNotAuthorized($response);
    }

    public function testAccessIsAllowedWhenAuthorizationSucceeds(): void
    {
        $response = $this->getJson('/googleApps/user/nicknames/v3/tepeds?token=fakeToken&domain=' . $this->domain);

        $this->assertRequestIsAuthorized($response);
    }


    public function testCanGetUserNicknames(): void
    {
        $this->withToken('fakeToken')
            ->withoutAcceptHeader()
            ->willAuthenticateUser();

        $response = $this->getJson('/googleApps/user/nicknames/v3/tepeds?token=fakeToken&domain=' . $this->domain);
        $this->assertRouteName('googleApps.user.nicknames.v3.read');

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'owner',
                    'nickname',
                ],
            ]);
    }

    public function testResponseUnauthorizedWhenNoTokenProvided(): void
    {
        $response = $this->getJson('/googleApps/user/nicknames/v3/bob');
        $response->assertStatus(401);
    }

    public function assertRequestIsNotAuthorized(TestResponse $response): void
    {
        $response->assertStatus(App::API_UNAUTHORIZED);
    }

}
