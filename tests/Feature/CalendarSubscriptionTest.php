<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/8/18
 * Time: 7:51 PM
 */

namespace Tests\Feature;

use Illuminate\Foundation\Testing\TestResponse;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Exception\BadRequest;

class CalendarSubscriptionTest extends GoogleAppsTestCase
{

    private $calendarId;

    public function setUp(): void
    {
        parent::setUp();

        if($this->domain == 'gtest.miamioh.edu') {
            $this->calendarId = 'gtest.miamioh.edu_sgihgag6u094mqokea2rofb02g@group.calendar.google.com';
        } elseif ($this->domain == 'gdev.miamioh.edu') {
            $this->calendarId = 'gdev.miamioh.edu_ffefq4ep1uhqmhl8mtbv2e3nv4@group.calendar.google.com';
        } else {
            throw new \Exception('This domain not currently supported in tests!');
        }

    }

    public function testAuthenticatedCalendarSubscriptionResourceIsNotAuthenticatedWithInvalidToken(): void
    {
        $this->willNotAuthenticateUser();

        $response = $this->getJson('/googleApps/calendarSubscription/v3/bob');

        $this->assertRequestIsNotAuthorized($response);
    }

    public function testResponseUnauthorizedWhenNoTokenProvided(): void
    {
        $response = $this->getJson('/googleApps/calendarSubscription/v3/bob');

        $response->assertStatus(401);
    }

    public function testAccessIsDeniedWhenAuthorizationFails(): void
    {
        $this->willNotAuthorizeUser();

        $response = $this->getJson('/googleApps/calendarSubscription/v3/' . $this->user . '?token=fakeToken&domain=' . $this->domain);

        $this->assertRequestIsNotAuthorized($response);
    }

    public function testAccessIsAllowedWhenAuthorizationSucceeds(): void
    {
        $response = $this->getJson('/googleApps/calendarSubscription/v3/' . $this->user . '?token=fakeToken&domain=' . $this->domain);

        $this->assertRequestIsAuthorized($response);
    }

    public function testCanGetCalendarSubscription(): void
    {
        $response = $this->getJson('/googleApps/calendarSubscription/v3/' . $this->user . '?token=fakeToken&domain=' . $this->domain);

        $this->assertRouteName('googleApps.calendarSubscription.v3.read');
        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    [
                        'Name',
                        'ID',
                        'AccessLevel',
                        'TimeZone',
                        'Hidden',
                        'Selected',
                        'Color'
                    ],
                ],
            ]);
    }

    public function testCanCreateCalendarSubscription(): void
    {
        $this->assertCalendarSubscriptionHasBeenCreated([]);
    }

    public function testCanDeleteCalendarSubscription(): void
    {

        // Create the calendar we're going to delete
        $temporarySubscription = $this->callCreateCalendarSubscription(
            [
                'uniqueId' => $this->user,
                'calendarId' => $this->calendarId,
                'hidden' => false,
                'selected' => false,
            ]
        );

        $response = $this->deleteJson('/googleApps/calendarSubscription/v3/' .
            $this->calendarId . '/' . $this->user .
            '?token=fakeToken&domain=' . $this->domain);

        // Tests that the delete route was called
        // $this->assertAction('delete')
        $this->assertRouteName('googleApps.calendarSubscription.v3.delete');

        $response->assertStatus(200);

    }

    public function testCannotCreateCalendarSubscriptionWithInvalidDataModel(): void
    {
        $invalidDataModel =
            [
                'Name' => 'MiamiOH events',
                'ID' => 'testCalendar@dgev.miamioh.edu',
                'AccessLevel' => 'reader',
                'TimeZone' => 'America/New_York',
                'Hidden' => false,
                'Selected' => false,
                'Color' => '#9a9cff',
            ];

        $response = $this->postJson('/googleApps/calendarSubscription/v3?token=fakeToken&domain=' . $this->domain, $invalidDataModel);

        $response->assertStatus(App::API_BADREQUEST);
    }

    public function testCannotCreateCalendarSubscriptionWithInvalidColor(): void
    {
        $invalidDataModel =
            [
                'id' => 'tugooglesdk',
                'calendar' => $this->calendarId,
                'hidden' => false,
                'selected' => false,
                'color' => 'invalidColor'
            ];

        $response = $this->postJson('/googleApps/calendarSubscription/v3?token=fakeToken&domain=' . $this->domain, $invalidDataModel);

        $response->assertStatus(App::API_BADREQUEST);
    }

    public function testCannotGetCalendarSubscriptionsWithInvalidUniqueId(): void
    {
        $response = $this->getJson('/googleApps/calendarSubscription/v3/invalidUniqueId?token=fakeToken');

        $response->assertStatus(App::API_BADREQUEST);
    }

    public function testCannotCreateCalendarSubscriptionWithInvalidUniqueId():void
    {
        $response = $this->callCreateCalendarSubscription([
            'id' => 'invalidUniqueId'
        ]);

        $response->assertStatus(App::API_BADREQUEST);
    }

    public function testCannotDeleteCalendarSubscriptionWithInvalidUniqueId():void
    {
        // Create the calendar we're going to delete
        $this->callCreateCalendarSubscription(
            [
                'id' => 'tugooglesdk',
            ]
        );

        $response = $this->deleteJson('/googleApps/calendarSubscription/v3/' .
            $this->calendarId . '/invalidUniqueId?token=fakeToken&domain=' .
            $this->domain);

        $response->assertStatus(App::API_BADREQUEST);
    }



    /*
     * Helper Methods
     */

    // Asserts that a Subscription has been created
    private function assertCalendarSubscriptionHasBeenCreated(array $subscriptionData = []): void
    {
        $response = $this->callCreateCalendarSubscription($subscriptionData);

        $this->assertRouteName('googleApps.calendarSubscription.v3.create');

        $response->assertStatus(App::API_CREATED)
        ->assertJsonStructure([
            'data' => [
                    'uniqueId',
                    'calendarId'
            ],
        ]);
    }

    // Creates a new Calendar Subscription and returns the response
    private function callCreateCalendarSubscription($overrides = []): TestResponse
    {
        $subscriptionData = array_merge(
            [
                'id' => $this->user,
                'calendar' => $this->calendarId,
                'hidden' => false,
                'selected' => false,
            ]
        , $overrides);

        return $this->postJson('/googleApps/calendarSubscription/v3?token=fakeToken&domain=' . $this->domain, $subscriptionData);
    }



}