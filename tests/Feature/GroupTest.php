<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/8/18
 * Time: 7:51 PM
 */

namespace Tests\Feature;

use Illuminate\Foundation\Testing\TestResponse;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Service\CredentialValidator\CredentialValidatorInterface;
use MiamiOH\RESTng\Testing\UsesAuthentication;
use Tests\TestCase;

class GroupTest extends GoogleAppsTestCase
{

    private $groupID;

    public function setUp(): void
    {
        parent::setUp();

        $this->groupID = 'testingGroup@' . $this->domain;

//        $this->showExceptions();
    }

    public function testAuthenticatedGroupResourceIsNotAuthorizedWithInvalidToken(): void
    {
        $this->willNotAuthenticateUser();

        $response = $this->getJson('/googleApps/group/v3/fakeGroupId');

        $this->assertRequestIsNotAuthorized($response);
    }

    public function testAccessIsDeniedWhenAuthorizationFails(): void
    {
        $this->willNotAuthorizeUser();

        $response = $this->getJson('/googleApps/group/v3/'. $this->groupID . '?token=fakeToken');

        $this->assertRequestIsNotAuthorized($response);
    }

    public function testAccessIsAllowedWhenAuthorizationSucceeds(): void
    {
        $response = $this->getJson('/googleApps/group/v3/'. $this->groupID . '?token=fakeToken');

        $this->assertRequestIsAuthorized($response);
    }


    public function testCanGetGroup(): void
    {
        $response = $this->getJson('/googleApps/group/v3/'. $this->groupID . '?token=fakeToken');

        $this->assertRouteName('googleApps.group.v3.read');
        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'groupId',
                    'groupName',
                    'description',
                    'directMembers',
                    'adminCreated',
                ],
            ]);
    }

    public function testCanDeleteAndCreateGroup(): void
    {
        $response = $this->deleteJson('/googleApps/group/v3/'. $this->groupID . '?token=fakeToken');

        // Tests that the delete route was called
        $this->assertRouteName('googleApps.group.v3.delete');

        $response->assertStatus(200);

        // Re-create the group
        $this->assertGroupHasBeenCreated([]);

    }

    public function testResponseUnauthorizedWhenNoTokenProvided(): void
    {
        $response = $this->getJson('/googleApps/group/v3/fakeGroupId');
        $response->assertStatus(401);
    }

    private function assertGroupHasBeenCreated(array $groupData = []): void
    {
        $response = $this->callCreateGroup($groupData);

        $this->assertRouteName('googleApps.group.v3.create');

        $response->assertStatus(App::API_CREATED)
            ->assertJsonStructure([
                'data' => [
                    'groupId',
                    'groupName',
                    'description',
                ],
            ]);
    }

    public function assertRequestIsNotAuthorized(TestResponse $response): void
    {
        $response->assertStatus(App::API_UNAUTHORIZED);
    }

    private function callCreateGroup($overrides = []): TestResponse
    {
        $groupData = array_merge([
            [
                'id' => $this->groupID,
                'name' => 'OneThroughSix',
                'description' => 'test group',
            ]
        ], $overrides);

        return $this->postJson('/googleApps/group/v3?token=fakeToken', $groupData);
    }

    public function testCannotCreateGroupWithInvalidDataModel(): void
    {
        $invalidDataModel = [
            [
                'groupId' => '123456@gdev.miamioh.edu',
                'groupName' => 'OneThroughSix',
                'restOfParametersMissing' => true,
            ],
        ];

        $response = $this->postJson('/googleApps/group/v3?token=fakeToken', $invalidDataModel);

        $response->assertStatus(App::API_BADREQUEST);
    }

}