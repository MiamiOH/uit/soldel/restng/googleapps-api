<?php
/**
 * Created by PhpStorm.
 * User: rotunomp
 * Date: 6/19/18
 * Time: 2:51 PM
 */

namespace Tests\Feature;

use Illuminate\Foundation\Testing\TestResponse;
use MiamiOH\GoogleApps\Api\GoogleAppsDatabaseTestCase;
use MiamiOH\GoogleApps\Api\GoogleGroupSourceService;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Exception\BadRequest;
use MiamiOH\RESTng\Service\Apm\Transaction;
use PHPUnit\Framework\MockObject\MockObject;

class UserGroupMembershipTest extends GoogleAppsDatabaseTestCase
{
    private $groupId;

    public function setUp(): void
    {
        parent::setUp();

        $this->groupId = 'testingGroupUser@' . $this->domain;
        $this->user = 'testuser2';

//        $this->showExceptions();
    }

    public function testAuthenticatedUserGroupMembershipResourceIsNotAuthorizedWithInvalidToken(): void
    {
        $this->willNotAuthenticateUser();

        $response = $this->getJson('/googleApps/user/groupMembership/v3/bob');

        $this->assertRequestIsNotAuthorized($response);
    }

    public function testAccessIsDeniedWhenAuthorizationFails(): void
    {
        $this->willNotAuthorizeUser();

        $response = $this->getJson('/googleApps/user/groupMembership/v3/' . $this->user . '?token=fakeToken&domain=' . $this->domain);

        $this->assertRequestIsNotAuthorized($response);
    }

    public function testAccessIsAllowedWhenAuthorizationSucceeds(): void
    {
        $this->ensureTestUserIsReady();

        $response = $this->getJson('/googleApps/user/groupMembership/v3/' . $this->user . '?token=fakeToken&domain=' . $this->domain);

        $this->assertRequestIsAuthorized($response);
    }

    public function testCanGetUserGroupMembership(): void
    {
        $this->ensureTestUserIsReady();

        $response = $this->getJson('/googleApps/user/groupMembership/v3/' . $this->user . '?token=fakeToken&domain=' . $this->domain);

//        $this->assertRouteName('googleApps.user.groupMembership.v3.read');
        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    [
                        'groupId',
                        'groupName',
                        'description',
                        'directMember',
                        'mu_population_method',
                    ],
                ],
            ]);
    }

    public function testCanCreateAndDeleteUserGroupMembership(): void
    {
        $this->ensureTestUserIsReady();

        $response = $this->deleteJson('/googleApps/user/groupMembership/v3/' . $this->groupId . '/' . $this->user . '?token=fakeToken');//        $this->assertRouteName('googleApps.user.groupMembership.v3.delete');
        $response->assertStatus(200);

        $response = $this->callCreateUserGroupMembership();

        $this->assertUserGroupMembershipHasBeenCreated($response);
    }
    
    public function testResponseUnauthorizedWhenNoTokenProvided(): void
    {
        $response = $this->getJson('/googleApps/user/groupMembership/v3/bob');
        $response->assertStatus(401);
    }

    public function testEmptyUserGroupMembershipWithInvalidUniqueId(): void
    {
        $response = $this->getJson('/googleApps/user/groupMembership/v3/invalidUserId?token=fakeToken&domain=' . $this->domain);

        $response->assertStatus(App::API_OK);
        $response->assertJsonFragment(['data' => []]);
    }

    private function assertUserGroupMembershipHasBeenCreated(TestResponse $response): void
    {
        $this->assertRouteName('googleApps.user.groupMembership.v3.create');

        $response->assertStatus(App::API_CREATED)
            ->assertJsonStructure([
                'data'
            ]);
    }

    private function callCreateUserGroupMembership($overrides = []): TestResponse
    {
        $membershipData = array_merge(
            [
                'memberId' => $this->user,
                'groupId' => $this->groupId,
            ]
        , $overrides);

        return $this->postJson('/googleApps/user/groupMembership/v3?token=fakeToken', $membershipData);
    }

    public function testCannotCreateUserGroupMembershipWithInvalidDataModel(): void
    {
        $invalidDataModel = [
            [
                'groupId' => '12345@gdev.miamioh.edu',
                'groupName' => 'Friends and Fans of Richard Starkey',
                'description' => '1234 5678910',
                'directMember' => true,
                'mu_population_method' => 'rtag',
            ],
            [
                'parameter1' => 'gggggg',
                'parameter2' => 'hhhhhh',
                'parameter3' => 'iiiiii'
            ]
        ];

        $response = $this->postJson('/googleApps/user/groupMembership/v3?token=fakeToken', $invalidDataModel);

        $response->assertStatus(App::API_BADREQUEST);
    }

    private function ensureTestUserIsReady(): void
    {
        /** @var Transaction $transaction */
        $transaction = $this->app->getService('ApmTransaction');

        // check if $this->user exists
        $response = $this->getJson('/googleApps/user/v3/' . $this->user.'?token=fakeToken&domain=' . $this->domain);
        $transaction->restart();

        // create if not
        if (!$response->isSuccessful()) {
            $this->postJson('/googleApps/user/v3?token=fakeToken', [
                'username' => $this->user,
                'givenName' => 'API Testing',
                'familyName' => 'Groups',
                'password' => 'cherryBomb'
            ]);
            $transaction->restart();
        }
        //check if $this->user is member of $this->groupId
        $response = $this->getJson('/googleApps/user/groupMembership/v3/' . $this->user . '?token=fakeToken&domain=' . $this->domain);
        $transaction->restart();

        $memberships = json_decode($response->getContent(), true);

        if (!empty($memberships['data'])) {
            foreach ($memberships['data'] as $membership) {
                if (!empty($membership['groupId']) && $membership['groupId'] === $this->groupId) {
                    return;
                }
            }
        }
        // add if not
        $this->callCreateUserGroupMembership();
    }

}
