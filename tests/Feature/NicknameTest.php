<?php
/**
 * Created by PhpStorm.
 * User: rotunomp
 * Date: 6/21/18
 * Time: 3:03 PM
 */

namespace Tests\Feature;

use Illuminate\Foundation\Testing\TestResponse;
use MiamiOH\RESTng\App;

class NicknameTest extends GoogleAppsTestCase
{
    private $nickname;

    public function setUp(): void
    {
        parent::setUp();

        $this->nickname = 'dstepe';
    }

    public function testAuthenticatedNicknameResourceIsNotAuthorizedWithInvalidToken(): void
    {
        $this->willNotAuthenticateUser();

        $response = $this->getJson('/googleApps/nickname/v3/bob?token=fakeToken');


        $this->assertRequestIsNotAuthorized($response);
    }

    public function testAccessIsDeniedWhenAuthorizationFails(): void
    {
        $this->willNotAuthorizeUser();

        $response = $this->getJson('/googleApps/nickname/v3/' . $this->user . '?token=fakeToken&domain=' . $this->domain);

        $this->assertRequestIsNotAuthorized($response);
    }

    public function testAccessIsAllowedWhenAuthorizationSucceeds(): void
    {
        $response = $this->getJson('/googleApps/nickname/v3/' . $this->user . '?token=fakeToken&domain=' . $this->domain);

        $this->assertRequestIsAuthorized($response);
    }

    public function testCanGetNickname(): void
    {
        $response = $this->getJson('/googleApps/nickname/v3/' . $this->user . '?token=fakeToken&domain=' . $this->domain);
        $this->assertRouteName('googleApps.nickname.v3.read');

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    [
                        'owner',
                        'nickname',
                    ],
                ],
            ]);
    }

    public function testCanCreateNickname(): void
    {
        $this->assertNicknameHasBeenCreated([]);
    }

    public function testCanDeleteNickname(): void
    {
        $response = $this->deleteJson('/googleApps/nickname/v3/' . $this->nickname . '?token=fakeToken');
        $this->assertRouteName('googleApps.nickname.v3.delete');

        $response->assertStatus(200);
    }

    public function testResponseUnauthorizedWhenNoTokenProvided(): void
    {
        $response = $this->getJson('/googleApps/nickname/v3/bob');
        $response->assertStatus(401);
    }

    private function assertNicknameHasBeenCreated(array $nicknameData = []): void
    {
        $response = $this->callCreateNickname($nicknameData);
        $this->assertRouteName('googleApps.nickname.v3.create');

        $response->assertStatus(App::API_CREATED)
            ->assertJsonStructure([
                'data' => [

                        'owner',
                        'nickname'

                ],
            ]);
    }

    public function assertRequestIsNotAuthorized(TestResponse $response): void
    {
        $response->assertStatus(App::API_UNAUTHORIZED);
    }

    private function callCreateNickname($overrides = []): TestResponse
    {
        $nicknameData = array_merge([
                'owner' => $this->user,
                'nickname' => $this->nickname,
        ], $overrides);

        return $this->postJson('/googleApps/nickname/v3?token=fakeToken&domain=' . $this->domain, $nicknameData);
    }

    public function testCannotCreateNicknameWithInvalidDataModel(): void
    {
        $invalidDataModel = [
            [
                'param' => 'hhhh',
            ]
        ];

        $response = $this->postJson('/googleApps/nickname/v3?token=fakeToken', $invalidDataModel);

        $response->assertStatus(App::API_BADREQUEST);
    }

}
