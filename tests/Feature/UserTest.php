<?php

namespace Tests\Feature;


use Illuminate\Foundation\Testing\TestResponse;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Exception\BadRequest;

class UserTest extends GoogleAppsTestCase
{
    public function setUp(): void
    {
        parent::setUp();

//        $this->showExceptions();
    }

    public function testAuthenticatedUserResourceIsNotAuthorizedWithInvalidToken(): void
    {
        $this->willNotAuthenticateUser();

        $response = $this->getJson('/googleApps/user/v3/bob?token=fakeToken');

        $this->assertRequestIsNotAuthorized($response);
    }

    public function testAccessIsDeniedWhenAuthorizationFails(): void
    {
        $this->willNotAuthorizeUser();

        $response = $this->getJson(
            '/googleApps/user/v3/' . $this->user.'?token=fakeToken&domain=' . $this->domain);

        $this->assertRequestIsNotAuthorized($response);
    }

    public function testAccessIsAllowedWhenAuthorizationSucceeds(): void
    {
        $response = $this->getJson(
            '/googleApps/user/v3/' . $this->user.'?token=fakeToken&domain=' . $this->domain);

        $this->assertRequestIsAuthorized($response);
    }

    public function testCanGetUserInfo(): void
    {
        $response = $this->getJson(
            '/googleApps/user/v3/' . $this->user.'?token=fakeToken&domain=' . $this->domain);

        $this->assertRouteName('googleApps.user.v3.read');

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'username',
                    'firstname',
                    'lastname',
                    'suspended',
                    'suspensionReason',
                    'admin',
                    'mustChangePassword',
                    'agreedToTerms'
                ],
            ]);
    }

    public function testCanCreateUser(): void
    {
        // Ensure the test user has been deleted before trying to create it.
        $this->deleteJson('/googleApps/user/v3/testuser1?token=fakeToken');
        try {
            $response = $this->callCreateUser(['username' => 'testuser1']);

            $this->assertUserHasBeenCreated($response);
        } catch (\Exception $e) {
            print_r("\nThe user " . $this->user . " could not be created because: \n");
            print_r($e->getMessage() . "\n");
        }
    }

    public function testCanUpdateUserPassword(): void
    {
        $response = $this->putJson(
            '/googleApps/user/v3/' . $this->user.'?token=fakeToken&domain=' . $this->domain,
            [ 'password' => 'Hello1234!']
        );
        $this->assertRouteName('googleApps.user.v3.update');

        $response->assertStatus(200);

    }

    public function testCanUpdateUserInfo(): void
    {
        $updateData = [
            'givenName' => 'Tom',
            'suspend' => true
        ];

        $response = $this->putJson('/googleApps/user/v3/' . $this->user.'?token=fakeToken&domain=' . $this->domain, $updateData);

        $this->assertRouteName('googleApps.user.v3.update');
        $response->assertStatus(200);
    }

    public function testResponseUnauthorizedWhenNoTokenProvided(): void
    {
        $response = $this->getJson('/googleApps/user/v3/bob');
        $response->assertStatus(401);
    }

    public function testCannotGetUserInfoWithInvalidUniqueId(): void
    {
        $response = $this->getJson('/googleApps/user/v3/invalidUserId123?token=fakeToken');

        $response->assertStatus(App::API_NOTFOUND);
    }

//    public function testCannotCreateUserWithInvalidUniqueId(): void
//    {
//        $fakeUserData = [
//            'username' => 'invalidUserId',
//            'givenName' => 'Tyler',
//            'familyName' => 'Okonma',
//            'password' => 'cherryBomb'
//        ];
//
//        $response = $this->postJson('/googleApps/user/v3?token=fakeToken', $fakeUserData);
//
//        $response->assertStatus(App::API_BADREQUEST);
//    }

    public function testCannotUpdateUserWithInvalidUniqueId(): void
    {
        $updateData = [
            'givenName' => 'Tom',
            'suspend' => true,
            'familyName' => 'Jones'
        ];

        $response = $this->putJson('/googleApps/user/v3/invalidUniqueId123?token=fakeToken&domain=' . $this->domain, $updateData);

        $response->assertStatus(App::API_NOTFOUND);
    }

    public function testCannotUpdateBothPasswordAndOtherInfo(): void
    {
        $updateData = [
            'password' => 'password1',
            'givenName' => 'Tom',
            'suspend' => true
        ];

        $response = $this->putJson('/googleApps/user/v3/' . $this->user.'?token=fakeToken&domain=' . $this->domain, $updateData);

        $response->assertStatus(App::API_BADREQUEST);
    }

    public function testCannotUpdateInfoWithInvalidDataModel(): void
    {
        $updateData = [
            'nothingOfSubstance' => 'qwertyuiop',
            'evenLess' => 'f'
        ];

        $response = $this->putJson('/googleApps/user/v3/' . $this->user.'?token=fakeToken&domain=' . $this->domain, $updateData);

        $response->assertStatus(App::API_BADREQUEST);
    }

    private function assertUserHasBeenCreated(TestResponse $response): void
    {
//        $this->assertRouteName('googleApps.user.v3.create');

        $response->assertStatus(App::API_CREATED)
            ->assertJsonStructure([
                'data' => [
                    'username',
                    'firstname',
                    'lastname',
                    'suspended',
                    'admin',
                    'mustChangePassword',
                    'agreedToTerms'
                ],
            ]);
    }

    public function assertRequestIsNotAuthorized(TestResponse $response): void
    {
        $response->assertStatus(App::API_UNAUTHORIZED);
    }

    private function callCreateUser($overrides = []): TestResponse
    {
        $userData = array_merge([
                'username' => $this->user,
                'givenName' => 'Tyler',
                'familyName' => 'Okonma',
                'password' => 'cherryBomb'
        ], $overrides);

        return $this->postJson('/googleApps/user/v3?token=fakeToken', $userData);
    }

    public function testCannotCreateUserWithInvalidDataModel(): void
    {
        $invalidDataModel = [
            [
                'groupId' => '12345@gdev.miamioh.edu',
                'groupName' => 'Friends and Fans of Richard Starkey',
                'description' => '1234 5678910',
                'directMember' => true,
                'mu_population_method' => 'rtag',
            ],
            [
                'parameter1' => 'gggggg',
                'parameter2' => 'hhhhhh',
                'parameter3' => 'iiiiii'
            ]
        ];

        $response = $this->postJson('/googleApps/user/v3?token=fakeToken', $invalidDataModel);

        $response->assertStatus(App::API_BADREQUEST);
    }

}