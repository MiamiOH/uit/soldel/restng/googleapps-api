<?php

namespace Tests\Feature;


use Illuminate\Foundation\Testing\TestResponse;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Testing\UsesAuthentication;
use Tests\TestCase;

class ProfileTest extends GoogleAppsTestCase
{

    public function setUp(): void
    {
        parent::setUp();

//        $this->showExceptions();
    }

    public function testAuthenticatedUserResourceIsNotAuthorizedWithInvalidToken(): void
    {
        $this->willNotAuthenticateUser();

        $response = $this->putJson('/googleApps/profile/v3/bob?token=fakeToken');

        $this->assertRequestIsNotAuthorized($response);
    }

    public function testAccessIsDeniedWhenAuthorizationFails(): void
    {
        $this->willNotAuthorizeUser();

        $response = $this->getJson('/googleApps/profile/v3/'.$this->user.'?token=fakeToken&domain='.$this->domain);

        $this->assertRequestIsNotAuthorized($response);
    }

    public function testAccessIsAllowedWhenAuthorizationSucceeds(): void
    {
        $response = $this->getJson('/googleApps/profile/v3/'.$this->user.'?token=fakeToken&domain='.$this->domain);

        $this->assertRequestIsAuthorized($response);
    }


    public function testCanGetProfile(): void
    {
        $response = $this->getJson('/googleApps/profile/v3/'.$this->user.'?token=fakeToken&domain='.$this->domain);

        $this->assertRouteName('googleApps.profile.v3.read');
        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'username',
                    'indexed'
                ],
            ]);
    }

    public function testCanUpdateProfile(): void
    {
        $response = $this->putJson('/googleApps/profile/v3/'.$this->user.'?token=fakeToken&domain='.$this->domain, ['indexed' => 'true']);
        $this->assertRouteName('googleApps.profile.v3.update');

        $response->assertStatus(200);

    }

    public function testResponseUnauthorizedWhenNoTokenProvided(): void
    {
        $response = $this->putJson('/googleApps/profile/v3/bob');
        $response->assertStatus(401);
    }

    public function assertRequestIsNotAuthorized(TestResponse $response): void
    {
        $response->assertStatus(App::API_UNAUTHORIZED);
    }

}
