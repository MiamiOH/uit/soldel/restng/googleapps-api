<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\TestResponse;
use MiamiOH\GoogleApps\Api\GoogleGroupSourceService;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Testing\UsesAuthentication;
use MiamiOH\RESTng\Testing\UsesAuthorization;
use Tests\TestCase;

class GoogleAppsTestCase extends TestCase
{

    use UsesAuthentication;
    use UsesAuthorization;
    protected $domain;
    protected $user;

    /** @var GoogleGroupSourceService|MockObject */
    private $groupSource;

    protected function setUp(): void
    {
        parent::setUp();

        $this->domain = env('GOOGLE_API_DOMAIN');
        $this->user = env('GOOGLE_API_USER');

        // Authenticate and Authorize user for all test cases (In the cases
        // Where we want the user to not be, it will be changed later)
        $this->withToken('fakeToken')
            ->withoutAcceptHeader()
            ->willAuthenticateUser();

        $this->willAuthorizeUser();

        $this->groupSource = $this->getMockBuilder(GoogleGroupSourceService::class)
            ->setMethods(['getGroupSource'])
            ->getMock();

        $this->groupSource->method('getGroupSource')->willReturn('rtag');

        $this->useService([
            'name' => 'GoogleGroupSourceService',
            'object' => $this->groupSource,
            'description' => 'Group source',
        ]);

    }

    protected function assertRequestIsAuthorized(TestResponse $response): void
    {
        $response->assertSuccessful();
    }

    protected function assertRequestIsNotAuthorized(TestResponse $response): void
    {
        $response->assertStatus(App::API_UNAUTHORIZED);
    }

}