<?php
/**
 * Created by PhpStorm.
 * User: rotunomp
 * Date: 6/14/18
 * Time: 3:18 PM
 */

namespace MiamiOH\GoogleApps\Api;


use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\Response;

class GroupService extends GoogleServiceHelper
{

    public function getGroupForGroupId(): Response
    {
        $response = $this->getResponse();
        $request = $this->getRequest();
        $groupId = $request->getResourceParam('groupId');
        $options = $request->getOptions();
        $domain = array_key_exists('domain', $options) ? $options['domain'] : $this->getDomain();
        $group = [];

        if(!strpos($groupId,$domain)){
            $groupId = $groupId.'@'.$domain;
        }

        $groupService = $this->getGroupServiceClient();

        try {
            $groupResponse = $groupService->groups->get($groupId);
        } catch (\Exception $e) {
            $response->setStatus($e->getCode());
            $response->setPayload([$e->getMessage()]);
            return $response;
        }

        $group = [
            'groupId' => $groupResponse->email,
            'groupName' => $groupResponse->name,
            'description' => $groupResponse->description,
            'directMembers' => $groupResponse->directMembersCount,
            'adminCreated' => $groupResponse->adminCreated
        ];

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload( $group );

        return $response;
    }

    public function createGroup (): Response
    {
        $response = $this->getResponse();
        $request = $this->getRequest();
        $options = $request->getOptions();
        $domain = array_key_exists('domain', $options) ? $options['domain'] : $this->getDomain();
        $group = [];
        $data = $request->getData();

        try {
            $this->validateDataModel($data);
        } catch (\Exception $e) {
            $response->setStatus(App::API_BADREQUEST);
            $response->setPayload([$e->getMessage()]);
            return $response;
        }

        $groupService = $this->getGroupServiceClient();

        // The object we're sending
        $newGroup = new \Google_Service_Directory_Group();

        $idWithDomain=$data[0]['id'];
        if(!strpos($idWithDomain,$domain)){
            $idWithDomain = $data[0]['id'].'@'.$domain;
        }

        $newGroup->setEmail($idWithDomain);
        $newGroup->setName($data[0]['name']);
        $newGroup->setDescription($data[0]['description']);

        try {
            $groupResponse = $groupService->groups->insert($newGroup);
        } catch (\Exception $e) {
            $response->setStatus($e->getCode());
            $response->setPayload([$e->getMessage()]);
            return $response;
        }

        $group = [
            'groupId' => $groupResponse->email,
            'groupName' => $groupResponse->name,
            'description' => $groupResponse->description,
        ];

        $response->setStatus(\MiamiOH\RESTng\App::API_CREATED);
        $response->setPayload($group);

        return $response;
    }

    public function deleteGroup(): Response
    {
        $response = $this->getResponse();
        $request = $this->getRequest();
        $groupId = $request->getResourceParam('groupId');
        $options = $request->getOptions();
        $domain = array_key_exists('domain', $options) ? $options['domain'] : $this->getDomain();

        if(!strpos($groupId,$domain)){
            $groupId = $groupId.'@'.$domain;
        }

        $groupService = $this->getGroupServiceClient();

        try {
            $groupResponse = $groupService->groups->delete($groupId);
        } catch (\Exception $e) {
            $response->setStatus($e->getCode());
            $response->setPayload([$e->getMessage()]);
            return $response;
        }

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);

        return $response;
    }

    // Returns an authenicated Client for interacting with the Google API
    private function getGroupServiceClient(): \Google_Service_Directory
    {
        $client = $this->getGoogleClient();

        $client->setSubject($this->getAdminUser());

        $groupService = new \Google_Service_Directory($client);
        return $groupService;
    }

    /**
     * @param array $inputData
     * @throws \Exception
     * This function iterates through some data and makes sure the data model
     * is correct. Should a member of the data model be missing, an
     * exception is thrown.
     */
    private function validateDataModel(array $inputData): void
    {
        foreach ($inputData as $datum) {
            if (!isset($datum['id'])) {
                throw new \Exception('Invalid data model!');
            } elseif (!isset($datum['name'])) {
                throw new \Exception('Invalid data model!');
            }
        }

    }
}