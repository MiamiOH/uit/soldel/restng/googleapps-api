<?php
/**
 * Created by PhpStorm.
 * User: rotunomp
 * Date: 6/21/18
 * Time: 1:31 PM
 */

namespace MiamiOH\GoogleApps\Api;


use MiamiOH\RESTng\Util\ResourceProvider;

class NicknameResourceProvider extends ResourceProvider
{

    public function registerDefinitions(): void
    {
        $this->addDefinition(array(
            'name' => 'Nickname',
            'type' => 'object',
            'properties' => array(
                'owner' => array(
                    'type' => 'string',
                ),
                'nickname' => array(
                    'type' => 'string',
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => 'Nickname.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/Nickname'
            )
        ));
    }

    public function registerServices(): void
    {
        $this->addService(array(
           'name' => 'NicknameService',
           'class' => \MiamiOH\GoogleApps\Api\NicknameService::class,
           'description' => 'provides nickname services.',
        ));
    }

    public function registerResources(): void
    {
        //Line 520 GET
        $this->addResource(array(
            'action' => 'read',
            'description' => 'Get nicknames for a user.',
            'name' => 'googleApps.nickname.v3.read',
            'pattern' => '/googleApps/nickname/v3/:uniqueId',
            'service' => 'NicknameService',
            'method' => 'getNicknameForUser',
            'params' => array(
                'uniqueId' => array('description' => 'The unique ID of the user'),
            ),
            'options' => array(
                'domain' => array(
                    'required' => false,
                    'default' => '',
                    'description' => 'Domain',
                )
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'A Nickname information object',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/Nickname',
                    )
                ),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array('application' => 'GoogleApps',
                        'module' => 'Nickname',
                        'key' => 'read'
                    ),
                ),
            ),
        ));

        // Line 558 POST
        $this->addResource(array(
            'action' => 'create',
            'description' => 'Create new nickname.',
            'name' => 'googleApps.nickname.v3.create',
            'pattern' => '/googleApps/nickname/v3',
            'service' => 'NicknameService',
            'method' => 'createNickname',
            'params' => array(),
            'body' => array(
                'description' => 'A nickname object',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/Nickname'
                ),
            ),
            'options' => array(
                'domain' => array(
                    'required' => false,
                    'default' => '',
                    'description' => 'Domain',
                )
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_CREATED => array(
                    'description' => 'A nickname to be added',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/Nickname',
                    )
                ),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array('application' => 'GoogleApps',
                        'module' => 'Nickname',
                        'key' => 'write'
                    ),
                ),
            ),
        ));

        // Line 616 DELETE
        $this->addResource(array(
            'action' => 'delete',
            'name' => 'googleApps.nickname.v3.delete',
            'description' => 'Deletes the given Nickname for a user.',
            'pattern' => '/googleApps/nickname/v3/:nickname',
            'service' => 'NicknameService',
            'method' => 'deleteNickname',
            'params' => array(
                'nickname' => array('description' => 'The nickname to remove'),
            ),
            'options' => array(
                'domain' => array(
                    'required' => false,
                    'default' => '',
                    'description' => 'Domain',
                )
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'Confirmation that the delete was performed',
                ),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array('application' => 'GoogleApps',
                        'module' => 'Nickname',
                        'key' => 'write'
                    ),
                ),
            ),
        ));

    }

    public function registerOrmConnections(): void
    {
    }
}