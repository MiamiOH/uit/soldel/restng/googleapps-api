<?php


namespace MiamiOH\GoogleApps\Api;


use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Exception\BadRequest;
use MiamiOH\RESTng\Service;
use MiamiOH\RESTng\Util\Response;

class ProfileService extends GoogleServiceHelper
{

    public function getProfile(): Response
    {
        $response = $this->getResponse();
        $request = $this->getRequest();
        $uniqueId = $request->getResourceParam('uniqueId');
        $options = $request->getOptions();
        $domain = array_key_exists('domain', $options) ? $options['domain'] : $this->getDomain();
        $profile = [];

        $profileService = $this->getProfileServiceClient();

        try {
            $profileResponse = $profileService->users->get($uniqueId . '@' . $domain);
        } catch (\Exception $e) {
            $response->setStatus($e->getCode());
            $response->setPayload([$e->getMessage()]);
            return $response;
        }

        $profile = [
            'username' => str_replace('@'.$domain,'',$profileResponse->primaryEmail),
            'indexed' => $profileResponse->includeInGlobalAddressList? 'true': 'false'
        ];

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload( $profile );

        return $response;


    }

    public function updateProfileInfo(): Response
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $uniqueId = $request->getResourceParam('uniqueId');
        $data = $request->getData();
        $options = $request->getOptions();
        $domain = array_key_exists('domain', $options) ? $options['domain'] : $this->getDomain();

        if(!isset($data['indexed'])) {
            $response->setStatus(App::API_BADREQUEST);
            $response->setPayload("Body must contain indexed!");
            return $response;
        }

        $profileService = $this->getProfileServiceClient();

        try {
            $user = $profileService->users->get($uniqueId . '@' . $domain);
        } catch (\Exception $e) {
            $response->setStatus($e->getCode());
            $response->setPayload([$e->getMessage()]);
            return $response;
        }

        $user->includeInGlobalAddressList = $data['indexed'];

        try {
            $updateResponse = $profileService->users->update($uniqueId . '@' . $domain, $user);
        } catch (\Exception $e) {
            $response->setStatus($e->getCode());
            $response->setPayload([$e->getMessage()]);
            return $response;
        }

        $returnArray = [
            'username' => str_replace('@'.$domain,'',$updateResponse->primaryEmail),
            'indexed' => $updateResponse->includeInGlobalAddressList? 'true': 'false'
        ];

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($returnArray);

        return $response;

    }

    private function getProfileServiceClient(): \Google_Service_Directory
    {
        $client = $this->getGoogleClient();

        $client->setSubject($this->getAdminUser());

        $profileService = new \Google_Service_Directory($client);

        return $profileService;
    }

}