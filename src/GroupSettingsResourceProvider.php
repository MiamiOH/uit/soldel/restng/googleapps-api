<?php


namespace MiamiOH\GoogleApps\Api;


use MiamiOH\RESTng\Util\ResourceProvider;

class GroupSettingsResourceProvider extends ResourceProvider
{


    public function registerDefinitions(): void
    {
        $this->addDefinition(array(
            'name' => 'GroupSettings',
            'type' => 'object',
            'properties' => array(
                'allow_external_members' => array(
                    'type' => 'boolean',
                ),
                'allow_google_communication' => array(
                    'type' => 'boolean',
                ),
                'allow_web_posting' => array(
                    'type' => 'boolean',
                ),
                'archive_only' => array(
                    'type' => 'boolean',
                ),
                'custom_reply_to' => array(
                    'type' => 'string',
                ),
                'default_message_deny_notification_text' => array(
                    'type' => 'string',
                ),
                'description' => array(
                    'type' => 'string',
                ),
                'email' => array(
                    'type' => 'string',
                ),
                'include_in_global_address_list' => array(
                    'type' => 'boolean',
                ),
                'is_archived' => array(
                    'type' => 'boolean',
                ),
                'max_message_bytes' => array(
                    'type' => 'integer',
                ),
                'members_can_post_as_the_group' => array(
                    'type' => 'boolean',
                ),
                'message_display_font' => array(
                    'type' => 'string',
                ),
                'message_moderation_level' => array(
                    'type' => 'string',
                ),
                'name' => array(
                    'type' => 'string',
                ),
                'reply_to' => array(
                    'type' => 'string',
                ),
                'send_message_deny_notification' => array(
                    'type' => 'boolean',
                ),
                'show_in_group_directory' => array(
                    'type' => 'boolean',
                ),
                'spam_moderation_level' => array(
                    'type' => 'string',
                ),
                'who_can_contact_owner' => array(
                    'type' => 'string',
                ),
                'who_can_invite' => array(
                    'type' => 'string',
                ),
                'who_can_join' => array(
                    'type' => 'string',
                ),
                'who_can_leave_group' => array(
                    'type' => 'string',
                ),
                'who_can_post_message' => array(
                    'type' => 'string',
                ),
                'who_can_view_group' => array(
                    'type' => 'string',
                ),
                'who_can_view_membership' => array(
                    'type' => 'string',
                ),
                'mu_population_method' => array(
                    'type' => 'string',
                ),
            )
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'GroupSettingsService',
            'class' => \MiamiOH\GoogleApps\Api\GroupSettingsService::class,
            'description' => 'Provides group settings services.',
            'set' => array(
                'googleGroupSource' => array('type' => 'service', 'name' => 'GoogleGroupSourceService')
            )
        ));
    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'description' => 'Get group settings info.',
            'name' => 'googleApps.group.settings.v3.read',
            'pattern' => '/googleApps/group/settings/v3/:groupId',
            'service' => 'GroupSettingsService',
            'method' => 'getGroupSettingsInfo',
            'params' => array(
                'groupId' => array('description' => 'The group ID'),
            ),
            'options' => array(),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'GoogleGroupSourceService Settings info',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/GroupSettings',
                    )
                ),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array('application' => 'GoogleApps',
                        'module' => 'Group',
                        'key' => 'read'
                    ),
                ),
            ),
        ));

        // Line 333 PUT
        $this->addResource(array(
            'action' => 'update',
            'name' => 'googleApps.group.settings.v3.update',
            'description' => 'Updates the given group object.',
            'pattern' => '/googleApps/group/settings/v3/:groupId',
            'service' => 'GroupSettingsService',
            'method' => 'updateGroupSettingsInfo',
            'body' => array(
                'description' => 'A group settings object. Partial PUTs are supported.',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/GroupSettings'
                ),
            ),
            'params' => array(
                'groupId' => array('description' => 'The group ID of the user'),
            ),
            'options' => array(
                'domain' => array(
                    'required' => false,
                    'default' => '',
                    'description' => 'Domain',
                )
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array('application' => 'GoogleApps',
                        'module' => 'Group',
                        'key' => 'write'
                    ),
                ),
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'Confirmation that the update was performed',
                ),
            ),
        ));
    }

    public function registerOrmConnections(): void
    {
    }
}