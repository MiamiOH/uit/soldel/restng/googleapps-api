<?php


namespace MiamiOH\GoogleApps\Api;


use MiamiOH\RESTng\Connector\DatabaseFactory;
use MiamiOH\RESTng\Connector\DataSourceFactory;
use MiamiOH\RESTng\Connector\LDAPFactory;
use MiamiOH\RESTng\Legacy\LDAP;

class GoogleGroupSourceService
{

    /** @var DatabaseFactory */
    private $databaseFactory;


    public function getGroupSource(string $groupId): string
    {
        $groupId = preg_replace('/@.*$/', '', $groupId);

        $dbh = $this->databaseFactory->getHandle('muconnect');

        $source = $dbh->queryfirstcolumn('
			select source
				from group_info
				where shortname = lower(?)
			', $groupId);

        switch ($source) {
            case 'm':
                return 'manual';

            case 'r':
                return 'rtag';

            default:
                return '';
        }
        #return $source == 'm' ? 'manual' : 'rtag';

    }

    public function setdatabasefactory(DatabaseFactory $databaseFactory)
    {
        $this->databaseFactory = $databaseFactory;
    }
}