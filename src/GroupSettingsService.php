<?php
/**
 * Created by PhpStorm.
 * User: rotunomp
 * Date: 6/19/18
 * Time: 1:08 PM
 */

namespace MiamiOH\GoogleApps\Api;


use MiamiOH\RESTng\Util\Response;

class GroupSettingsService extends GoogleServiceHelper
{

    /** @var GoogleGroupSourceService */
    private $googleGroupSource;

    public function getGroupSettingsInfo(): Response
    {
        $response = $this->getResponse();
        $request = $this->getRequest();
        $groupId = $request->getResourceParam('groupId');
        $groupSettings = [];

        $groupSettingsService = $this->getGroupSettingsServiceClient($groupId);

        $optparams = array('alt' => 'json');
        try {
            $groupSettingsResponse = $groupSettingsService->groups->get($groupId . '@' . $this->getDomain(), $optparams);
        } catch (\Exception $e) {
            $response->setStatus($e->getCode());
            $response->setPayload([$e->getMessage()]);
            return $response;
        }

        //Unfortunately, the keys for the new Google API do not match the keys in our API
        $NewToOldMapping = array(
            "email" => "email",
            "name" => "name",
            "description" => "description",
            "whoCanJoin" => "who_can_join",
            "whoCanViewMembership" => "who_can_view_membership",
            "whoCanViewGroup" => "who_can_view_group",
            "whoCanInvite" => "who_can_invite",
            "allowExternalMembers" => "allow_external_members",
            "whoCanPostMessage" => "who_can_post_message",
            "allowWebPosting" => "allow_web_posting",
            "maxMessageBytes" => "max_message_bytes",
            "isArchived" => "is_archived",
            "archiveOnly" => "archive_only",
            "messageModerationLevel" => "message_moderation_level",
            "spamModerationLevel" => "spam_moderation_level",
            "replyTo" => "reply_to",
            "customReplyTo" => "custom_reply_to",
            "sendMessageDenyNotification" => "send_message_deny_notification",
            "defaultMessageDenyNotificationText" => "default_message_deny_notification_text",
            "showInGroupDirectory" => "show_in_group_directory",
            "allowGoogleCommunication" => "allow_google_communication",
            "membersCanPostAsTheGroup" => "members_can_post_as_the_group",
            "messageDisplayFont" => "message_display_font",
            "includeInGlobalAddressList" => "include_in_global_address_list",
            "whoCanLeaveGroup" => "who_can_leave_group",
            "whoCanContactOwner" => "who_can_contact_owner"
        );
        
        foreach ($groupSettingsResponse as $key => $value) {
            if (array_key_exists($key, $NewToOldMapping)) {
                $groupSettings[$NewToOldMapping[$key]] = $value;
            }
        }

        // TODO: Make sure the getGroupSource method is working
        $groupSettings['mu_population_method'] = $this->googleGroupSource->getGroupSource($groupId);

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload( $groupSettings );

        return $response;

    }


    public function updateGroupSettingsInfo(): Response
    {
        $response = $this->getResponse();
        $request = $this->getRequest();
        $groupId = $request->getResourceParam('groupId');
        $options = $request->getOptions();
        $domain = array_key_exists('domain', $options) ? $options['domain'] : $this->getDomain();
        $data = $request->getData();

        //Gather up the changes to be made to the group settings:
        $updatedAttributes = array_keys($data);
        $arguments = [];
        $requireGroupSettingsAPI = array(
            'who_can_join',
            'who_can_view_membership',
            'who_can_view_group',
            'who_can_invite',
            'allow_external_members',
            'who_can_post_message',
            'allow_web_posting',
            'max_message_bytes',
            'is_archived',
            'archive_only',
            'message_moderation_level',
            'spam_moderation_level',
            'reply_to',
            'custom_reply_to',
            'send_message_deny_notification',
            'default_message_deny_notification_text',
            'show_in_group_directory',
            'allow_google_communication',
            'members_can_post_as_the_group',
            'message_display_font',
            'include_in_global_address_list',
            'who_can_leave_group',
            'who_can_contact_owner',
            'name',
            'description',
        );
        $useGroupSettingsAPI = array_intersect($requireGroupSettingsAPI, $updatedAttributes);
        if ($useGroupSettingsAPI) {
            // Collect incoming parameters.  The ones listed here are straight value inputs.  We
            // will handle special cases next.
            $paramList = array(
                'description',
                'who_can_join',
                'who_can_view_membership',
                'who_can_view_group',
                'who_can_invite',
                'allow_external_members',
                'who_can_post_message',
                'allow_web_posting',
                'max_message_bytes',
                'is_archived',
                'archive_only',
                'message_moderation_level',
                'spam_moderation_level',
                'reply_to',
                'custom_reply_to',
                'send_message_deny_notification',
                'default_message_deny_notification_text',
                'show_in_group_directory',
                'allow_google_communication',
                'members_can_post_as_the_group',
                'message_display_font',
                'include_in_global_address_list',
                'who_can_leave_group',
                'who_can_contact_owner',
                'name',
            );

            //populate $arguments with the values that need to change.
            $arguments = array();
            foreach ($paramList as $paramName) {
                if (isset($data[$paramName]) && $data[$paramName]) {
                    $arguments[$paramName] = $data[$paramName];
                }
            }

            /*
                Coming from Exchange, we are used a positive assertion to hide groups.  Google
                uses a positive assertation to show groups.  If the calling app uses the Google
                attributes, they are directly mapped.  If the mu_hide_group is used, we need to
                invert the logic.  If the calling app does not specify 'true' or 'false', just
                pass in what was given.
            */
            if (isset($data['mu_hide_group'])) {
                $indicator = '';
                switch (strtolower($data['mu_hide_group'])) {
                    case 'false':
                        $indicator = 'true';
                        break;
                    case 'true':
                        $indicator = 'false';
                        break;
                    default:
                        $indicator = $data['mu_hide_group'];
                        break;
                }
                $arguments['include_in_global_address_list'] = $indicator;
                $arguments['show_in_group_directory'] = $indicator;
            }
        }

        $client = $this->getGoogleClient();
        $client->setSubject($this->getAdminUser());

        $groupService = new \Google_Service_Groupssettings($client);
        $optparams = array('alt' => 'json');

        if(!strpos($groupId,$domain)){
            $groupId = $groupId.'@'.$domain;
        }

        //Get the group object first.
        try {
            $groupObject = $groupService->groups->get($groupId, $optparams);
        } catch (\Exception $e) {
            $response->setStatus($e->getCode());
            $response->setPayload([$e->getMessage()]);
            return $response;
        }

        //Call a helper method to set the object variables based on what is changing.
        $this->setSettingsObjectFromArgs($groupObject, $arguments);

        //Make the update.
        try {
            $groupService->groups->update($groupId, $groupObject, $optparams);
        } catch (\Exception $e) {
            $response->setStatus($e->getCode());
            $response->setPayload([$e->getMessage()]);
            return $response;
        }

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);

        return $response;
    }

    // Returns an authenicated Client for interacting with the Google API
    private function getGroupSettingsServiceClient(
        string $uniqueId = null,
        string $domain = null): \Google_Service_Groupssettings
    {
        $client = $this->getGoogleClient();

        // Impersonate the user we are getting data for
        $userToImpersonate = $uniqueId . '@' . $domain;
//        $client->setSubject($userToImpersonate);
        $client->setSubject($this->getAdminUser());

        $userGroupMembershipService = new \Google_Service_Groupssettings($client);
        return $userGroupMembershipService;
    }

    public function setGoogleGroupSource(GoogleGroupSourceService $googleGroupSource)
    {
        $this->googleGroupSource = $googleGroupSource;
    }

    private function setSettingsObjectFromArgs($groupObjectFromGoogle, $arguments){

        foreach($arguments as $key => $value){
            switch ($key){
                case 'allow_external_members':
                    $groupObjectFromGoogle->allowExternalMembers = $value;
                    break;
                case 'who_can_join':
                    $groupObjectFromGoogle->whoCanJoin = strtoupper($value);
                    break;
                case 'who_can_view_membership':
                    $groupObjectFromGoogle->whoCanViewMembership = strtoupper($value);
                    break;
                case 'who_can_view_group':
                    $groupObjectFromGoogle->whoCanViewGroup = strtoupper($value);
                    break;
                case 'who_can_invite':
                    $groupObjectFromGoogle->whoCanInvite = strtoupper($value);
                    break;
                case 'who_can_post_message':
                    $groupObjectFromGoogle->whoCanPostMessage = strtoupper($value);
                    break;
                case 'allow_web_posting':
                    $groupObjectFromGoogle->allowWebPosting = $value;
                    break;
                case 'max_message_bytes':
                    $groupObjectFromGoogle->maxMessageBytes = strtoupper($value);
                    break;
                case 'is_archived':
                    $groupObjectFromGoogle->isArchived = $value;
                    break;
                case 'archive_only':
                    $groupObjectFromGoogle->archiveOnly = $value;
                    break;
                case 'message_moderation_level':
                    $groupObjectFromGoogle->messageModerationLevel = strtoupper($value);
                    break;
                case 'spam_moderation_level':
                    $groupObjectFromGoogle->spamModerationLevel = strtoupper($value);
                    break;
                case 'reply_to':
                    $groupObjectFromGoogle->replyTo = strtoupper($value);
                    break;
                case 'custom_reply_to':
                    $groupObjectFromGoogle->customReplyTo = $value;
                    break;
                case 'send_message_deny_notification':
                    $groupObjectFromGoogle->sendMessageDenyNotification = $value;
                    break;
                case 'default_message_deny_notification_text':
                    $groupObjectFromGoogle->defaultMessageDenyNotificationText = $value;
                    break;
                case 'show_in_group_directory':
                    $groupObjectFromGoogle->showInGroupDirectory = $value;
                    break;
                case 'allow_google_communication':
                    $groupObjectFromGoogle->allowGoogleCommunication = $value;
                    break;
                case 'members_can_post_as_the_group':
                    $groupObjectFromGoogle->membersCanPostAsTheGroup = $value;
                    break;
                case 'message_display_font':
                    $groupObjectFromGoogle->messageDisplayFont = strtoupper($value);
                    break;
                case 'include_in_global_address_list':
                    $groupObjectFromGoogle->includeInGlobalAddressList = $value;
                    break;
                case 'who_can_contact_owner':
                    $groupObjectFromGoogle->whoCanContactOwner =strtoupper($value);
                    break;
                case 'who_can_leave_group':
                    $groupObjectFromGoogle->whoCanLeaveGroup = strtoupper($value);
                    break;
                case 'name':
                    $groupObjectFromGoogle->name = $value;
                    break;
                case 'description':
                    $groupObjectFromGoogle->description = $value;
                    break;
                default:
                    break;
            }
        }
    }
}