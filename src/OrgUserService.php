<?php


namespace MiamiOH\GoogleApps\Api;


use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Exception\BadRequest;
use MiamiOH\RESTng\Util\Response;

class OrgUserService extends GoogleServiceHelper
{

    public function createOrgUser (): Response
    {
        $response = $this->getResponse();
        $request = $this->getRequest();
        $options = $request->getOptions();
        $requestBody = $request->getData();
        $domain = array_key_exists('domain', $options) ? $options['domain'] : $this->getDomain();
        $returnArray = [];

        try {
            $this->validateDataModel($requestBody);
        } catch (\Exception $e) {
            $response->setStatus(App::API_BADREQUEST);
            $response->setPayload([$e->getMessage()]);
            return $response;
        }
        $user = $requestBody['user'];
        $orgUnit = $requestBody['orgUnit'];

        $orgUnitService = $this->getOrgUserServiceClient();

        try {
            $userEntry = $orgUnitService->users->get($user . '@' . $domain);
        } catch (\Exception $e) {
            $response->setStatus($e->getCode());
            $response->setPayload([$e->getMessage()]);
            return $response;
        }
        $userEntry -> setOrgUnitPath('/'.$orgUnit); //in the new API, the path requires "/" in front

        try {
            $orgUnitService->users->patch($user . '@' . $domain, $userEntry);
        } catch (\Exception $e) {
            $response->setStatus($e->getCode());
            $response->setPayload([$e->getMessage()]);
            return $response;
        }

        // Set the values for our return array

        $returnArray = [
            'username' => $user,
            'orgUnit' => $orgUnit,
        ];

        $response->setStatus(\MiamiOH\RESTng\App::API_CREATED);
        $response->setPayload($returnArray);

        return $response;
    }


    // Returns an authenicated Client for interacting with the Google API
    private function getOrgUserServiceClient(): \Google_Service_Directory
    {
        $client = $this->getGoogleClient();

        $client->setSubject($this->getAdminUser());

        $calendarService = new \Google_Service_Directory($client);

        return $calendarService;
    }

    /**
     * @param array $inputData
     * @throws \Exception
     * This function iterates through some data and makes sure the data model
     * is correct. Should a member of the data model be missing, an
     * exception is thrown.
     */
    private function validateDataModel(array $inputData): void
    {
        if (!isset($inputData['user'])) {
            throw new BadRequest('Body must contain user!');
        } elseif (!isset($inputData['orgUnit'])) {
            throw new BadRequest('Body must contain orgUnit!');
        }
    }

}