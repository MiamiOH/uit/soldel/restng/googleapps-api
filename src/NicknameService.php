<?php
/**
 * Created by PhpStorm.
 * User: rotunomp
 * Date: 6/21/18
 * Time: 2:43 PM
 */

namespace MiamiOH\GoogleApps\Api;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Exception\BadRequest;
use MiamiOH\RESTng\Util\Response;

class NicknameService extends GoogleServiceHelper
{

    public function getNicknameForUser(): Response
    {
        $response = $this->getResponse();
        $request = $this->getRequest();
        $uniqueId = $request->getResourceParam('uniqueId');
        $options = $request->getOptions();
        $domain = array_key_exists('domain', $options) ? $options['domain'] : $this->getDomain();
        $nicknames = [];
        $nicknameService = null;

        $nicknameService = $this->getNicknameServiceClient($uniqueId, $domain);

        $nicknameFull = ( !strpos($uniqueId, $domain)
            ? $uniqueId .'@'. $domain : $domain);

        try {
            $nicknameResponse = $nicknameService->users->get($nicknameFull);
        } catch (\Exception $e) {
            $response->setStatus($e->getCode());
            $response->setPayload([$e->getMessage()]);
            return $response;
        }

        $username = substr($nicknameResponse->primaryEmail, 0, strpos($nicknameResponse->primaryEmail, '@'));

        $nicknames[] = array(
            'owner' => $username,
            'nickname' => $uniqueId
        );

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload( $nicknames );

        return $response;
    }

    public function createNickname (): Response
    {
        $request = $this->getRequest();
        $options = $request->getOptions();
        $response = $this->getResponse();
        $requestBody = $request->getData();
        $domain = array_key_exists('domain', $options) ? $options['domain'] : $this->getDomain();

        // Check to see if the body has a valid data model before continuing
        try {
            $this->validateDataModel($requestBody);
        } catch (\Exception $e) {
            $response->setStatus(App::API_BADREQUEST);
            $response->setPayload([$e->getMessage()]);
            return $response;
        }
        $owner = $requestBody['owner'];
        $nickname = $requestBody['nickname'];

        $calendarService = $this->getNicknameServiceClient($owner, $domain);

        // The object we're sending
        $nicknameEntry = new \Google_Service_Directory_Alias;

        $nicknameFull = ( !strpos($nickname, $domain)
            ? $nickname .'@'. $domain : $domain);
        $nicknameEntry->alias = $nicknameFull;

        // Insert
        $ownerFull = (!strpos($owner, $domain) ? $owner.'@'.$domain :  $owner);

        try {
            $calendarService->users_aliases->insert($ownerFull, $nicknameEntry);
        } catch (\Exception $e) {
            $response->setStatus($e->getCode());
            $response->setPayload([$e->getMessage()]);
            return $response;
        }

        // Our return content
        $response->setStatus(\MiamiOH\RESTng\App::API_CREATED);
        $response->setPayload(array(
            'owner' => $owner,
            'nickname' => $nickname
        ));

        return $response;
    }

    public function deleteNickname(): Response
    {
        $response = $this->getResponse();
        $request = $this->getRequest();
        $nickname = $request->getResourceParam('nickname');
        $options = $request->getOptions();
        $domain = array_key_exists('domain', $options) ? $options['domain'] : $this->getDomain();

        $nicknameService = $this->getNicknameServiceClient(null, $domain);

        // We have to get the uniqueID from the nickname
        $nicknameFull = ( !strpos($nickname, $domain) ? $nickname.'@'.$domain : $nickname );

        try {
            $googleNicknameUser = $nicknameService->users->get($nicknameFull);
        } catch (\Exception $e) {
            $response->setStatus($e->getCode());
            $response->setPayload([$e->getMessage()]);
            return $response;
        }
        if(isset($googleNicknameUser->primaryEmail)){
            $nicknameEntry = new \Google_Service_Directory_Alias();
            $nicknameEntry->setAlias($nicknameFull);
            try{
                $nicknameResponse = $nicknameService->users_aliases->delete($googleNicknameUser->primaryEmail, $nicknameFull);
            } catch (\Exception $e) {
                $response->setStatus($e->getCode());
                $response->setPayload([$e->getMessage()]);
                return $response;
            }
            if (!isset($nicknameResponse)) {
                throw new \Exception('Nickname not found');
            }
        }

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        return $response;
    }


    private function getNicknameServiceClient(
        string $uniqueId = null,
        string $domain): \Google_Service_Directory
    {
        $client = $this->getGoogleClient();

        // Impersonate the user we are getting data for
        $userToImpersonate = $uniqueId . '@' . $domain;
//        $client->setSubject($userToImpersonate);
        $client->setSubject($this->getAdminUser());

        $nicknameService = new \Google_Service_Directory($client);
        return $nicknameService;
    }

    /**
     * @param array $inputData
     * @throws \Exception
     * This function iterates through some data and makes sure the data model
     * is correct. Should a member of the data model be missing, an
     * exception is thrown.
     */
    private function validateDataModel(array $inputData): void
    {
        if (!isset($inputData['owner'])) {
            throw new BadRequest('Body must contain owner!');
        } elseif (!isset($inputData['nickname'])) {
            throw new BadRequest('Body must contain nickname!');
        }

    }

}