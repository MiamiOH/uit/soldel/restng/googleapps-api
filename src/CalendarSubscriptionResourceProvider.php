<?php
/**
 * Created by PhpStorm.
 * User: rotunomp
 * Date: 6/14/18
 * Time: 3:03 PM
 */

namespace MiamiOH\GoogleApps\Api;


use MiamiOH\RESTng\Util\ResourceProvider;

class CalendarSubscriptionResourceProvider extends ResourceProvider
{

    public function registerDefinitions(): void
    {
        $this->addDefinition(array(
            'name' => 'CalendarSubscription',
            'type' => 'object',
            'properties' => array(
                'Name' => array(
                    'type' => 'string',
                ),
                'ID' => array(
                    'type' => 'string',
                ),
                'AccessLevel' => array(
                    'type' => 'string',
                ),
                'TimeZone' => array(
                    'type' => 'string',
                ),
                'Hidden' => array(
                    'type' => 'boolean',
                ),
                'Selected' => array(
                    'type' => 'boolean',
                ),
                'Color' => array(
                    'type' => 'string',
                    'description' => '6-character hex'
                ),
            )
        ));

        // These aren't truly the data models, but it's more work right now than we're willing to fix
        $this->addDefinition(array(
            'name' => 'SubscriptionPostBody',
            'type' => 'object',
            'properties' => array(
                'uniqueId' => array(
                    'type' => 'string',
                ),
                'calendarId' => array(
                    'type' => 'string',
                ),
                'hidden' => array(
                    'type' => 'boolean',
                ),
                'selected' => array(
                    'type' => 'boolean',
                ),
                'color' => array(
                    'type' => 'string',
                    'description' => '6-character hex'
                ),
            )
        ));

        // Ditto.
        $this->addDefinition(array(
            'name' => 'SubscriptionPostResponse',
            'type' => 'object',
            'properties' => array(
                'uniqueId' => array(
                    'type' => 'string',
                ),
                'calendarId' => array(
                    'type' => 'string',
                ),
            )

        ));

        $this->addDefinition(array(
            'name' => 'CalendarSubscription.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/CalendarSubscription'
            )
        ));
    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'CalendarSubscriptionService',
            'class' => \MiamiOH\GoogleApps\Api\CalendarSubscriptionService::class,
            'description' => 'Provides calendar subscription services.',
        ));
    }

    public function registerResources(): void
    {
        // Line 585 GET
        $this->addResource(array(
            'action' => 'read',
            'description' => 'Get calendar subscriptions for a user.',
            'name' => 'googleApps.calendarSubscription.v3.read',
            'pattern' => '/googleApps/calendarSubscription/v3/:uniqueId',
            'service' => 'CalendarSubscriptionService',
            'method' => 'getSubscriptionsForUser',
            'params' => array(
                'uniqueId' => array('description' => 'The unique ID of the user'),
            ),
            'options' => array(
                'domain' => array(
                    'required' => false,
                    'default' => '',
                    'description' => 'Domain',
                )
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'A list of Calendar Subscriptions',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/CalendarSubscription.Collection',
                    )
                ),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array('application' => 'GoogleApps',
                        'module' => 'CalendarSubscription',
                        'key' => 'read'
                    ),
                ),
            ),
        ));

        // Line 626 POST
        $this->addResource(array(
            'action' => 'create',
            'description' => 'Create new subscription.',
            'name' => 'googleApps.calendarSubscription.v3.create',
            'pattern' => '/googleApps/calendarSubscription/v3',
            'service' => 'CalendarSubscriptionService',
            'method' => 'createSubscription',
            'params' => array(),
            'body' => array(
                'description' => 'A calendar subscription object',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/SubscriptionPostBody'
                ),
            ),
            'options' => array(
                'domain' => array(
                    'required' => false,
                    'default' => '',
                    'description' => 'Domain',
                )
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_CREATED => array(
                    'description' => 'The subscription that was added',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/SubscriptionPostResponse',
                    )
                ),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array('application' => 'GoogleApps',
                        'module' => 'CalendarSubscription',
                        'key' => 'write'
                    ),
                ),
            ),
        ));

        // Line 658 DELETE
        $this->addResource(array(
            'action' => 'delete',
            'name' => 'googleApps.calendarSubscription.v3.delete',
            'description' => 'Deletes the given Calendar Subscription for a user.',
            'pattern' => '/googleApps/calendarSubscription/v3/:calendarId/:uniqueId',
            'service' => 'CalendarSubscriptionService',
            'method' => 'deleteSubscription',
            'params' => array(
                'calendarId' => array('description' => 'A Calendar id'),
                'uniqueId' => array('description' => 'The unique ID of the user'),
            ),
            'options' => array(
                'domain' => array(
                    'required' => false,
                    'default' => '',
                    'description' => 'Domain',
                )
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'Confirmation that the delete was performed',
                ),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array('application' => 'GoogleApps',
                        'module' => 'CalendarSubscription',
                        'key' => 'write'
                    ),
                ),
            ),
        ));
    }

    public function registerOrmConnections(): void
    {
    }
}