<?php


namespace MiamiOH\GoogleApps\Api;


use MiamiOH\RESTng\Util\ResourceProvider;

class GroupResourceProvider extends ResourceProvider
{

    public function registerDefinitions(): void
    {
        $this->addDefinition(array(
            'name' => 'GoogleGroupSourceService',
            'type' => 'object',
            'properties' => array(
                'groupId' => array(
                    'type' => 'string',
                ),
                'groupName' => array(
                    'type' => 'string',
                ),
                'description' => array(
                    'type' => 'string',
                ),
                'directMembers' => array(
                    'type' => 'integer',
                ),
                'adminCreated' => array(
                    'type' => 'integer',
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => 'GroupPostBody',
            'type' => 'object',
            'properties' => array(
                'id' => array(
                    'type' => 'string',
                ),
                'name' => array(
                    'type' => 'string',
                )
            )
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'GroupService',
            'class' => \MiamiOH\GoogleApps\Api\GroupService::class,
            'description' => 'Provides group services.',
        ));

    }

    public function registerResources(): void
    {
        // Line 159 GET
        $this->addResource(array(
            'action' => 'read',
            'description' => 'Get group info.',
            'name' => 'googleApps.group.v3.read',
            'pattern' => '/googleApps/group/v3/:groupId',
            'service' => 'GroupService',
            'method' => 'getGroupForGroupId',
            'params' => array(
                'groupId' => array('description' => 'The group ID to get info for'),
            ),
            'options' => array(
                'domain' => array(
                    'required' => false,
                    'default' => '',
                    'description' => 'Domain',
                )
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'A group info object',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/GoogleGroupSourceService',
                    )
                ),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array('application' => 'GoogleApps',
                        'module' => 'Group',
                        'key' => 'read'
                    ),
                ),
            ),
        ));

        // Line 311 POST
        $this->addResource(array(
            'action' => 'create',
            'description' => 'Create new group.',
            'name' => 'googleApps.group.v3.create',
            'pattern' => '/googleApps/group/v3',
            'service' => 'GroupService',
            'method' => 'createGroup',
            'params' => array(),
            'body' => array(
                'description' => 'A group info object',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/GroupPostBody'
                ),
            ),
            'options' => array(
                'domain' => array(
                    'required' => false,
                    'default' => '',
                    'description' => 'Domain',
                )
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_CREATED => array(
                    'description' => 'group info to be added',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/GoogleGroupSourceService',
                    )
                ),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array('application' => 'GoogleApps',
                        'module' => 'Group',
                        'key' => 'write'
                    ),
                ),
            ),
        ));

        // Line 357 DELETE
        $this->addResource(array(
            'action' => 'delete',
            'name' => 'googleApps.group.v3.delete',
            'description' => 'Deletes the given GoogleGroupSourceService.',
            'pattern' => '/googleApps/group/v3/:groupId',
            'service' => 'GroupService',
            'method' => 'deleteGroup',
            'params' => array(
                'groupId' => array('description' => 'The group ID to be deleted'),
            ),
            'options' => array(
                'domain' => array(
                    'required' => false,
                    'default' => '',
                    'description' => 'Domain',
                )
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'Confirmation that the delete was performed',
                ),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array('application' => 'GoogleApps',
                        'module' => 'Group',
                        'key' => 'write'
                    ),
                ),
            ),
        ));
    }

    public function registerOrmConnections(): void
    {
    }

}