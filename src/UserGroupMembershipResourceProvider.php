<?php
/**
 * Created by PhpStorm.
 * User: rotunomp
 * Date: 6/15/18
 * Time: 3:02 PM
 */

namespace MiamiOH\GoogleApps\Api;


use MiamiOH\RESTng\Util\ResourceProvider;

class UserGroupMembershipResourceProvider extends ResourceProvider
{

    public function registerDefinitions(): void
    {
        $this->addDefinition(array(
            'name' => 'UserGroupMembership',
            'type' => 'object',
            'properties' => array(
                'groupId' => array(
                    'type' => 'string',
                ),
                'groupName' => array(
                    'type' => 'string',
                ),
                'description' => array(
                    'type' => 'string',
                ),
                'directMember' => array(
                    'type' => 'boolean',
                ),
                'mu_population_method' => array(
                    'type' => 'string',
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => 'UserGroupMembershipPostBody',
            'type' => 'object',
            'properties' => array(
                'memberId' => array(
                    'type' => 'string',
                ),
                'groupId' => array(
                    'type' => 'string',
                ),
            )
        ));


        $this->addDefinition(array(
            'name' => 'UserGroupMembership.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/UserGroupMembership'
            )
        ));
    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'UserGroupMembershipService',
            'class' => \MiamiOH\GoogleApps\Api\UserGroupMembershipService::class,
            'description' => 'Provides user group membership services.',
            'set' => array(
                'googleGroupSource' => array('type' => 'service', 'name' => 'GoogleGroupSourceService')
            )
        ));
    }

    public function registerResources(): void
    {
        // Line 695 GET
        $this->addResource(array(
            'action' => 'read',
            'description' => 'Get user group memberships for a user.',
            'name' => 'googleApps.user.groupMembership.v3.read',
            'pattern' => '/googleApps/user/groupMembership/v3/:uniqueId',
            'service' => 'UserGroupMembershipService',
            'method' => 'getMembershipsForUser',
            'params' => array(
                'uniqueId' => array('description' => 'The unique ID of the user'),
            ),
            'options' => array(
                'domain' => array(
                    'required' => false,
                    'default' => '',
                    'description' => 'Domain',
                )
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'A list of GoogleGroupSourceService Memberships',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/UserGroupMembership.Collection',
                    )
                ),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array('application' => 'GoogleApps',
                        'module' => 'User',
                        'key' => 'read'
                    ),
                ),
            ),
        ));

        // Line 751 DELETE
        $this->addResource(array(
            'action' => 'delete',
            'name' => 'googleApps.user.groupMembership.v3.delete',
            'description' => 'Deletes the given GoogleGroupSourceService Membership for a user.',
            'pattern' => '/googleApps/user/groupMembership/v3/:groupId/:uniqueId',
            'service' => 'UserGroupMembershipService',
            'method' => 'deleteMembership',
            'params' => array(
                'groupId' => array('description' => 'A GoogleGroupSourceService id'),
                'uniqueId' => array('description' => 'The unique ID of the user'),
            ),
            'options' => array(
                'domain' => array(
                    'required' => false,
                    'default' => '',
                    'description' => 'Domain',
                )
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'Confirmation that the delete was performed',
                ),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array('application' => 'GoogleApps',
                        'module' => 'User',
                        'key' => 'write'
                    ),
                ),
            ),
        ));

        // Line 782 POST
        $this->addResource(array(
            'action' => 'create',
            'description' => 'Create new user group membership.',
            'name' => 'googleApps.user.groupMembership.v3.create',
            'pattern' => '/googleApps/user/groupMembership/v3',
            'service' => 'UserGroupMembershipService',
            'method' => 'createMembership',
            'params' => array(),
            'options' => array(
                'domain' => array(
                    'required' => false,
                    'default' => '',
                    'description' => 'Domain',
                )
            ),
            'body' => array(
                'description' => 'A user group membership object',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/UserGroupMembership'
                ),
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_CREATED => array(
                    'description' => 'A user group membership to be added',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/UserGroupMembership',
                    )
                ),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array('application' => 'GoogleApps',
                        'module' => 'User',
                        'key' => 'write'
                    ),
                ),
            ),
        ));
    }

    public function registerOrmConnections(): void
    {
    }
}