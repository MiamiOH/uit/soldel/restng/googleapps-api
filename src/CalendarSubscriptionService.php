<?php

namespace MiamiOH\GoogleApps\Api;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Exception\BadRequest;
use MiamiOH\RESTng\Util\Response;

class CalendarSubscriptionService extends GoogleServiceHelper
{

    const ALLOWED_COLORS = [
        '#A32929',
        '#B1365F',
        '#7A367A',
        '#5229A3',
        '#29527A',
        '#2952A3',
        '#1B887A',
        '#28754E',
        '#0D7813',
        '#528800',
        '#88880E',
        '#AB8B00',
        '#BE6D00',
        '#B1440E',
        '#865A5A',
        '#705770',
        '#4E5D6C',
        '#5A6986',
        '#4A716C',
        '#6E6E41',
        '#8D6F47',
        '#853104',
        '#691426',
        '#5C1158',
        '#23164E',
        '#182C57',
        '#060D5E',
        '#125A12',
        '#2F6213',
        '#2F6309',
        '#5F6B02',
        '#8C500B',
        '#8C500B',
        '#754916',
        '#6B3304',
        '#5B123B',
        '#42104A',
        '#113F47',
        '#333333',
        '#0F4B38',
        '#856508',
        '#711616'
    ];

    // Gets a list of calendar subscriptions
    public function getSubscriptionsForUser(): Response
    {
        $response = $this->getResponse();
        $request = $this->getRequest();
        $uniqueId = $request->getResourceParam('uniqueId');
        $options = $request->getOptions();
        $domain = array_key_exists('domain', $options) ? $options['domain'] : $this->getDomain();
        $subscriptions = [];

        $calendarService = $this->getCalendarServiceClient($uniqueId, $domain);

        try {
            $calendarResponse = $calendarService->calendarList->listCalendarList();
        } catch (\Exception $e) {
            $response->setStatus($e->getCode());
            $response->setPayload([$e->getMessage()]);
            return $response;
        }

        // Put the response in a formatted data array
        /** @var $item \Google_Service_Calendar_CalendarListEntry*/
        foreach ($calendarResponse->getItems() as $item) {
            $subscriptions[] = array(
                'Name' => $item->getSummary(),
                'ID' => $item->getId(),
                'AccessLevel' => $item->getAccessRole(),
                'TimeZone' => $item->getTimeZone(),
                'Hidden' => $item->getHidden() !== null,
                'Selected' => $item->getSelected() === 1,
                'Color' => $item->getBackgroundColor(),
            );
        }

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload( $subscriptions );

        return $response;
    }

    // Creates a new Calendar Subscription
    public function createSubscription (): Response
    {
        $request = $this->getRequest();
        $options = $request->getOptions();
        $response = $this->getResponse();
        $requestBody = $request->getData();
        $domain = array_key_exists('domain', $options) ? $options['domain'] : $this->getDomain();

        // Check to see if the body has a valid data model before continuing
        try {
            $this->validateDataModel($requestBody);
        } catch (\Exception $e) {
            $response->setStatus(App::API_BADREQUEST);
            $response->setPayload([$e->getMessage()]);
            return $response;
        }
        $uniqueId = $requestBody['id'];
        $calendarId = $requestBody['calendar'];

        $calendarService = $this->getCalendarServiceClient($uniqueId, $domain);

        // The object we are sending
        $calendarListEntry = new \Google_Service_Calendar_CalendarListEntry($uniqueId);
        $calendarListEntry->setId($calendarId);
        $calendarListEntry->setHidden(
            isset($requestBody['hidden']) ? $requestBody['hidden'] : false);
        $calendarListEntry->setSelected(
            isset($requestBody['selected']) ? $requestBody['selected'] : true);
        $calendarListEntry->setBackgroundColor(
            isset($requestBody['color']) ? $requestBody['color'] : '#9a9cff');
        $calendarListEntry->setForegroundColor('#000000');

        try {
            $creationResponse = $calendarService->calendarList->insert($calendarListEntry);
        } catch (\Exception $e) {
            $response->setStatus($e->getCode());
            $response->setPayload([$e->getMessage()]);
            return $response;
        }


        $response->setStatus(\MiamiOH\RESTng\App::API_CREATED);
        $response->setPayload([
            'uniqueId' => $uniqueId,
            'calendarId' => $creationResponse->getId(),
        ]);

        return $response;
    }

    // Deletes a Calendar Subscription based on Unique ID and Calendar ID
    public function deleteSubscription(): Response
    {
        $response = $this->getResponse();
        $request = $this->getRequest();
        $uniqueId = $request->getResourceParam('uniqueId');
        $calendarId = $request->getResourceParam('calendarId');
        $options = $request->getOptions();
        $domain = array_key_exists('domain', $options) ? $options['domain'] : $this->getDomain();

        $calendarService = $this->getCalendarServiceClient($uniqueId, $domain);

        try {
            $calendarResponse = $calendarService->calendarList->delete($calendarId);
        } catch (\Exception $e) {
            $response->setStatus($e->getCode());
            $response->setPayload([$e->getMessage()]);
            return $response;
        }


        $response->setStatus(\MiamiOH\RESTng\App::API_OK);

        return $response;
    }


    // Returns an authenicated Client for interacting with the Google API
    private function getCalendarServiceClient(
        string $uniqueId = null,
        string $domain = null): \Google_Service_Calendar
    {
        $client = $this->getGoogleClient();

        $userToImpersonate = $uniqueId . '@' . $domain;
        $client->setSubject($userToImpersonate);

        $calendarService = new \Google_Service_Calendar($client);

        return $calendarService;
    }

    /**
     * @param array $inputData
     * @throws \Exception
     * This function iterates through some data and makes sure the data model
     * is correct. Should a member of the data model be missing, an
     * exception is thrown.
     */
    private function validateDataModel(array $inputData): void
    {
        if (!isset($inputData['id'])) {
            throw new BadRequest('Body must contain uniqueId!');
        } elseif (!isset($inputData['calendar'])) {
            throw new BadRequest('Body must contain calendarId!');
        } elseif (isset($inputData['color']) && !in_array($inputData['color'],
                self::ALLOWED_COLORS)) {
            throw new BadRequest(
                "color must be one of " . implode(', ', self::ALLOWED_COLORS));
        }
    }

}