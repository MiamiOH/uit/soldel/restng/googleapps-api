<?php


namespace MiamiOH\GoogleApps\Api;


use MiamiOH\RESTng\Util\ResourceProvider;
use phpDocumentor\Reflection\Types\Array_;

class EmailSettingsResourceProvider extends ResourceProvider
{
    public function registerDefinitions(): void
    {
        // TODO: Create correct data models

        $this->addDefinition(array(
            'name' => 'EmailSettings',
            'type' => 'object',
            'properties' => array(
                'default' => array(
                    'type' => 'string',
                    'description' => 'yes or no'
                ),
                'address' => array(
                    'type' => 'string',
                ),
                'sendasid' => array(
                    'type' => 'string',
                ),
                'name' => array(
                    'type' => 'string',
                ),
                'replyto' => array(
                    'type' => 'string',
                ),
                'verified' => array(
                    'type' => 'string',
                    'description' => 'yes or no'
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => 'EmailSettingsSendAsPostBody',
            'type' => 'object',
            'properties' => array(
                'name' => array(
                    'type' => 'string',
                ),
                'address' => array(
                    'type' => 'string',
                ),
                'replyTo' => array(
                    'type' => 'string',
                ),
                'makeDefault' => array(
                    'type' => 'boolean',
                ),
            ),
        ));

        $this->addDefinition(array(
            'name' => 'EmailSettings.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/EmailSettings'
            )
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'EmailSettingsService',
            'class' => \MiamiOH\GoogleApps\Api\EmailSettingsService::class,
            'description' => 'Provides email settings services.',
        ));
    }

    public function registerResources(): void
    {
        // TODO: Implement correct data models
        // Line 66 GET
        $this->addResource(array(
            'action' => 'read',
            'description' => 'Get email settings for a user.',
            'name' => 'googleApps.emailSettings.v3.read',
            'pattern' => '/googleApps/emailSettings/v3/:uniqueId',
            'service' => 'EmailSettingsService',
            'method' => 'getEmailSettingsForUser',
            'params' => array(
                'uniqueId' => array('description' => 'The unique ID of the user'),
            ),
            'options' => array(
                'domain' => array(
                    'required' => false,
                    'default' => '',
                    'description' => 'Domain',
                )
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'A list of Email Settings',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/EmailSettings.Collection',
                    )
                ),
            ),
            'middleware' => [
                'authenticate' => ['type' => 'token'],
                'authorize' => array(
                    array('application' => 'GoogleApps',
                        'module' => 'EmailSettings',
                        'key' => 'read'
                    )
                )
            ],
        ));

        // GET for additional setting parameter
        $this->addResource(array(
            'action' => 'read',
            'description' => 'Get email settings for a user.',
            'name' => 'googleApps.emailSettings.v3.readWithSetting',
            'pattern' => '/googleApps/emailSettings/v3/:uniqueId/:setting',
            'service' => 'EmailSettingsService',
            'method' => 'getEmailSettingsForUser',
            'params' => array(
                'uniqueId' => array('description' => 'The unique ID of the user'),
                'setting' => array('description' => 'The setting we\'re looking for'),
            ),
            'options' => array(
                'domain' => array(
                    'required' => false,
                    'default' => '',
                    'description' => 'Domain',
                )
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'A list of Email Settings',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/EmailSettings.Collection',
                    )
                ),
            ),
            'middleware' => [
                'authenticate' => ['type' => 'token'],
                'authorize' => array(
                    array('application' => 'GoogleApps',
                        'module' => 'EmailSettings',
                        'key' => 'read'
                    )
                )
            ],
        ));


        // Line 103 POST
        $this->addResource(array(
            'action' => 'create',
            'description' => 'Create new email settings.',
            'name' => 'googleApps.emailSettings.v3.create',
            'pattern' => '/googleApps/emailSettings/v3/:uniqueId',
            'service' => 'EmailSettingsService',
            'method' => 'createSendAsAddress',
            'params' => array(
                'uniqueId' => array('description' => 'The unique ID of the user'),
            ),
            'options' => array(
                'domain' => array(
                    'required' => false,
                    'default' => '',
                    'description' => 'Domain',
                ),
            ),
            'body' => array(
                'description' => 'An email settings object',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/EmailSettingsSendAsPostBody'
                ),
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_CREATED => array(
                    'description' => 'Confirmation that the creation was successful',
                ),
            ),
            'middleware' => [
                'authenticate' => ['type' => 'token'],
                'authorize' => array(
                    array('application' => 'GoogleApps',
                        'module' => 'EmailSettings',
                        'key' => 'write'
                    )
                )
            ],
        ));


        // POST with additional setting for parameter
        $this->addResource(array(
            'action' => 'create',
            'description' => 'Create new email settings.',
            'name' => 'googleApps.emailSettings.v3.createWithSetting',
            'pattern' => '/googleApps/emailSettings/v3/:uniqueId/:setting',
            'service' => 'EmailSettingsService',
            'method' => 'createSendAsAddress',
            'params' => array(
                'uniqueId' => array('description' => 'The unique ID of the user'),
                'setting' => array('description' => 'The setting we\'re looking for'),
            ),
            'options' => array(
                'domain' => array(
                    'required' => false,
                    'default' => '',
                    'description' => 'Domain',
                )
            ),
            'body' => array(
                'description' => 'An email settings object',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/EmailSettingsSendAsPostBody'
                ),
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_CREATED => array(
                    'description' => 'Confirmation that the creation was successful',
                ),
            ),
            'middleware' => [
                'authenticate' => ['type' => 'token'],
                'authorize' => array(
                    array('application' => 'GoogleApps',
                        'module' => 'EmailSettings',
                        'key' => 'write'
                    )
                )
            ],
        ));

        // DELETE
        $this->addResource(array(
            'action' => 'delete',
            'description' => 'Delete a sendas address.',
            'name' => 'googleApps.emailSettings.v3.delete',
            'pattern' => '/googleApps/emailSettings/v3/:uniqueId/:sendAsEmail',
            'service' => 'EmailSettingsService',
            'method' => 'deleteSendAsAddress',
            'params' => array(
                'uniqueId' => array('description' => 'The unique ID of the user'),
                'sendAsEmail' => array('description' => 'The alias we\'re removing'),
            ),
            'options' => array(
                'domain' => array(
                    'required' => false,
                    'default' => '',
                    'description' => 'Domain',
                )
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'Confirmation that the deletion was successful',
                ),
            ),
            'middleware' => [
                'authenticate' => ['type' => 'token'],
                'authorize' => array(
                    array('application' => 'GoogleApps',
                        'module' => 'EmailSettings',
                        'key' => 'write'
                    )
                )
            ],
        ));


    }

    public function registerOrmConnections(): void
    {
    }
}