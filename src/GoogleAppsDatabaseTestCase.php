<?php


namespace MiamiOH\GoogleApps\Api;


use MiamiOH\RESTng\Connector\DatabaseFactory;
use MiamiOH\RESTng\Connector\LDAPFactory;
use MiamiOH\RESTng\Legacy\DB\DBH;
use Tests\Feature\GoogleAppsTestCase;

class GoogleAppsDatabaseTestCase extends GoogleAppsTestCase
{

    /** @var \MiamiOH\RESTng\Legacy\LDAP|\PHPUnit\Framework\MockObject\MockObject */
    private $ldap;

    /** @var \Dreamscapes\Ldap\Core\Result|\PHPUnit\Framework\MockObject\MockObject */
    private $ldapResult;

    public function setUp(): void
    {
        parent::setUp();


        $databaseFactory = $this->getMockBuilder(DatabaseFactory::class)
            ->setMethods(['getHandle'])
            ->getMock();

        $database = $this->getMockBuilder(DBH::class)
            ->setMethods(['queryfirstcolumn'])
            ->getMock();

        $database->method('queryfirstcolumn')
            ->willReturn('rtag');

        $databaseFactory->method('getHandle')
            ->willReturn($database);

        $this->app->useService([
            'name' => 'APIDatabaseFactory',
            'object' => $databaseFactory,
            'description' => 'Group source',
        ]);




//        $dataSourceFactory = $this->getMockBuilder(\MiamiOH\RESTng\Connector\DataSourceFactory::class)
//            ->setMethods(['getDataSource'])
//            ->getMock();
//
//        $dataSourceFactory->method('getDataSource')
//            ->willReturn(\MiamiOH\RESTng\Connector\DataSource::fromArray([
//                'name' => 'IDVault',
//                'type' => 'OCI8',
//                'database' => 'DMARS',
//            ]));
//
//        $this->app->useService([
//            'name' => 'APIDataSourceFactory',
//            'object' => $dataSourceFactory,
//            'description' => 'DataSources',
//        ]);

//        $ldapFactory = $this->getMockBuilder(\MiamiOH\RESTng\Connector\LDAPFactory::class)
//            ->setMethods(['getHandle'])
//            ->getMock();
//
//        $this->ldap = $this->getMockBuilder(\Dreamscapes\Ldap\Core\Ldap::class)
//            ->setMethods(['search', 'modAdd', 'modDelete'])
//            ->getMock();
//
//        $this->ldapResult = $this->getMockBuilder(\Dreamscapes\Ldap\Core\Result::class)
//            ->disableOriginalConstructor()
//            ->setMethods(['getEntries', '__destruct'])
//            ->getMock();
//
//        $this->ldap->method('search')->willReturn('rtag');
//
//        $ldapFactory->method('getHandle')->willReturn($this->ldap);
//
//
//        $this->app->useService([
//            'name' => 'APILDAPFactory',
//            'object' => $ldapFactory,
//            'description' => 'LDAP',
//        ]);

    }

}