<?php


namespace MiamiOH\GoogleApps\Api;


use MiamiOH\RESTng\Service;

abstract class GoogleServiceHelper extends Service
{
    // Returns an authenticated Google Client
    protected function getGoogleClient(array $scopes = []): \Google_Client
    {
        $scopes = array(
            'https://www.googleapis.com/auth/calendar',
            'https://www.googleapis.com/auth/apps.groups.settings',
            'https://www.googleapis.com/auth/admin.directory.user',
            'https://www.googleapis.com/auth/admin.directory.user.alias',
            'https://www.googleapis.com/auth/admin.directory.user.readonly',
            'https://www.googleapis.com/auth/admin.directory.group',
            'https://www.googleapis.com/auth/admin.directory.group.member',
            'https://www.googleapis.com/auth/admin.directory.group.member.readonly',
            'https://www.googleapis.com/auth/admin.directory.group.readonly',
            'https://www.googleapis.com/auth/admin.directory.orgunit',
            'https://www.googleapis.com/auth/gmail.settings.basic',
            'https://www.googleapis.com/auth/gmail.settings.sharing'
        );

        $client = new \Google_Client();
        $client->setApplicationName("MiamiGoogleAPIV3");

        // Using useApplicationDefaultCredentials
        putenv('GOOGLE_APPLICATION_CREDENTIALS=' . $this->getCredentials());
        $client->useApplicationDefaultCredentials();
        $client->addScope($scopes);
        $client->setAccessType('offline');

        if($client->isAccessTokenExpired()) {
            $client->getRefreshToken();
        }

        return $client;
    }

    private function getCredentials(): string
    {
        return env('GOOGLE_API_CREDENTIAL_FILE', 'google-service-account.json');
    }

    protected function getDomain(): string
    {
        return env('GOOGLE_API_DOMAIN', 'miamioh.edu');
    }

    protected function getAdminUser(): string
    {
        return env('GOOGLE_API_ADMIN_EMAIL', 'rtagusr@' . $this->getDomain());
    }

    protected function fixIdToIncludeDomain(string $idToCheck, string $domain): string
    {
        if(!strpos($idToCheck,$domain)){
            $idToCheck=$idToCheck.'@'.$domain;
        }

        return $idToCheck;
    }

}