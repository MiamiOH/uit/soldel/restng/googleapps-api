<?php


namespace MiamiOH\GoogleApps\Api;


use MiamiOH\RESTng\Util\ResourceProvider;

class CommonResourceProvider extends ResourceProvider
{


    public function registerDefinitions(): void
    {

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'GoogleGroupSourceService',
            'class' => \MiamiOH\GoogleApps\Api\GoogleGroupSourceService::class,
            'description' => 'Provides Google Group Source Resolution.',
            'set' => array(
                'databaseFactory' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
            )
        ));
    }

    public function registerResources(): void
    {

    }

    public function registerOrmConnections(): void
    {

    }
}