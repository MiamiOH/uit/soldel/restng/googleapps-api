<?php


namespace MiamiOH\GoogleApps\Api;


use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Service;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\Response;

class EmailSettingsService extends GoogleServiceHelper
{

    const ALLOWED_SETTINGS = [
        'sendas',
        'delegation',
    ];

    public function getEmailSettingsForUser(): Response
    {
        $response = $this->getResponse();
        $request = $this->getRequest();
        $uniqueId = $request->getResourceParam('uniqueId');
        $options = $request->getOptions();
        $domain = array_key_exists('domain', $options) ? $options['domain'] : $this->getDomain();

        $emailSettingsService = $this->getEmailSettingsServiceClient($uniqueId, $domain);

        $sendAs = $emailSettingsService->users_settings_sendAs->listUsersSettingsSendAs("me"); // the user id set to "me" just tells the service to get the information for whoever is the Google Client subject

        // Loop over the saveAs addresses and put them in an array
        $addresses = [];
        /** @var \Google_Service_Gmail_SendAs $item */
        foreach ($sendAs as $item) {
            $addresses[] = [
                    'default' => $item->getIsDefault() ? 'yes' : 'no',
                    'address' => $item->getSendAsEmail(),
                    'sendasid' => $item->getSendAsEmail(),
                    'name' => $item->getDisplayName(),
                    'replyto' => $item->getReplyToAddress(),
                    'verified' => $item->getVerificationStatus() ? 'yes' : 'no',
            ];
        }

        $addresses = [
            'sendas_addresses' => $addresses,
        ];

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload( $addresses );

        return $response;
    }

    public function createSendAsAddress(): Response
    {
        $request = $this->getRequest();
        $options = $request->getOptions();
        $response = $this->getResponse();
        $uniqueId = $request->getResourceParam('uniqueId');
        $requestBody = $request->getData();
        $domain = array_key_exists('domain', $options) ? $options['domain'] : $this->getDomain();

        // Check to see if the body has a valid data model before continuing
        try {
            $this->validateDataModel($requestBody);
        } catch (\Exception $e) {
            $response->setStatus(App::API_BADREQUEST);
            $response->setPayload([$e->getMessage()]);
            return $response;
        }

        $name = $requestBody['name'];
        $address = $requestBody['address'];
        $replyTo = $requestBody['replyTo'];
        $makeDefault = $requestBody['makeDefault'];

        $emailSettingsService = $this->getEmailSettingsServiceClient($uniqueId, $domain);

        $postBody = new \Google_Service_Gmail_SendAs();

        $postBody->setSendAsEmail($address);
        $postBody->setDisplayName($name);
        $postBody->setIsDefault($makeDefault);
        $postBody->setReplyToAddress($replyTo);

        $uniqueId = $this->fixIdToIncludeDomain($uniqueId, $domain);

        // Try to create the new sendas address
        try {
            $postResponse = $emailSettingsService->users_settings_sendAs->create($uniqueId, $postBody);
        } catch (\Exception $e) {
            $response->setStatus($e->getCode());
            $response->setPayload([$e->getMessage()]);
            return $response;
        }

        $response->setStatus(App::API_CREATED);
        return $response;

    }

    public function deleteSendAsAddress(): Response
    {
        $response = $this->getResponse();
        $request = $this->getRequest();
        $uniqueId = $request->getResourceParam('uniqueId');
        $sendAsEmail = $request->getResourceParam('sendAsEmail');
        $options = $request->getOptions();
        $domain = array_key_exists('domain', $options) ? $options['domain'] : $this->getDomain();

        $uniqueId = $this->fixIdToIncludeDomain($uniqueId, $domain);

        $emailSettingsService = $this->getEmailSettingsServiceClient($uniqueId, $domain);

        $deleteResponse = $emailSettingsService->users_settings_sendAs->delete($uniqueId, $sendAsEmail);


        $response->setStatus(App::API_OK);
        return $response;
    }

    // Returns an authenicated Client for interacting with the Google API
    private function getEmailSettingsServiceClient(
        string $uniqueId = null,
        string $domain = null): \Google_Service_Gmail
    {
        $client = $this->getGoogleClient();

        $uniqueId = $this->fixIdToIncludeDomain($uniqueId, $domain);

        $client->setSubject($uniqueId);
//        $client->setSubject($this->getAdminUser());

        $emailSettingsService = new \Google_Service_Gmail($client);
        return $emailSettingsService;
    }

    /**
     * @param array $inputData
     * @throws \Exception
     * This function iterates through some data and makes sure the data model
     * is correct. Should a member of the data model be missing, an
     * exception is thrown.
     */
    private function validateDataModel(array $inputData): void
    {
            if (!isset($inputData['name'])) {
                throw new \Exception('Invalid data model!');
            } elseif (!isset($inputData['address'])) {
                throw new \Exception('Invalid data model!');
        }

    }

    private function getEmailSettings(\Google_Service_Directory $emailSettingsService, string $url): array
    {
        $http_headers=array("Authorization" => "Bearer ".json_decode($emailSettingsService->getClient()->getAccessToken())->{'access_token'});

        // TODO: Figure out how to send a request manually
        $request = new \HttpRequest($url, 'POST', $http_headers);

        $batch = new \Google_Http_Batch($emailSettingsService->getClient());
        $batch->add($request);

        $response = $emailSettingsService->getClient()->execute($batch);




        // Format the response object
        //Create sendas_address collection
        $responseArray = [];
        if($response['feed']['entry']){
            //Process Each Sendas Entity
            foreach($response['feed']['entry'] as $i){
                $sendasItem= $i['apps$property'];
                //Check for ReplyTo
                if($sendasItem[1][value]){
                    $sendas = [];
                    //Process Each Item
                    foreach($sendasItem as $item){
                        if($item['name']=='isDefault'){
                            //Convert isDefault to default element and correct value
                            //Change true to yes and false to no
                            $sendas['default'] = ($item['value'] == 'true'?'yes':'no');
                        }else{
                            if($item['name']=='verified'){
                                $sendas[$item['name']] = ($item['value'] == 'true'?'yes':'no');
                            }else{
                                //All other Elements
                                $sendas[$item['name']] = $item['value'];
                            }
                        }
                    }

                    //Add Entity to responseArray
                    $responseArray[] = $sendas;
                }
            }
        }

        return $responseArray;
    }

    private function getDelegation(\Google_Service_Directory $emailSettingsService, string $url): array
    {
        $http_headers=array("Authorization" => "Bearer ".json_decode($emailSettingsService->getClient()->getAccessToken())->{'access_token'});

        // TODO: Figure out how to send a request manually
        $request = new \HttpRequest($url, 'POST', $http_headers);



        //Create sendas_address collection
        $responseArray = [];
        if($response != '' && isset($response['feed']['entry'])){
            //Process Each Sendas Entity
            if($response['feed']['entry']){
                foreach($response['feed']['entry'] as $i){
                    $sendasItem= $i['apps$property'];
                    //Check for ReplyTo
                    if($sendasItem[1][value]){
                        $delegations = [];
                        //Process Each Item
                        foreach($sendasItem as $item){
                            //All other Item
                            $delegations[strtolower($item['name'])] =  $item['value'];
                        }

                        //Add Entity to responseArray
                        $responseArray[] = $delegations;
                    }
                }
            }
        }

        return $responseArray;
    }

    private function postEmailSettings(\Google_Service_Directory $emailSettingsService, string $url, $data): array
    {
        $http_headers=array("Authorization" => "Bearer ".json_decode($emailSettingsService->getClient()->getAccessToken())->{'access_token'},
            "Content-Type" => "application/atom+xml");

        //Format POST Parametes for Googles API (atom+xml)
        $postParams= 	'<?xml version="1.0" encoding="utf-8"?>
							 <atom:entry xmlns:atom="http://www.w3.org/2005/Atom" xmlns:apps="http://schemas.google.com/apps/2006">'.
            '<apps:property name="name" value="'.$data['name'].'" />'.
            '<apps:property name="address" value="'.$data['address'].'" />'.
            '<apps:property name="replyTo" value="'.$data['replyTo'].'" />'.
            '<apps:property name="makeDefault" value="'.$data['makeDefault'].'" />'.
            '</atom:entry>';

        $request = new \HttpRequest($url, 'POST', $http_headers, $postParams);

        $request = new Request();
        $request->setData($data);

        $batch = new \Google_Http_Batch($emailSettingsService->getClient());
        $batch->add();

        $response = $emailSettingsService->getClient()->execute($batch);





    }

    private function getConfig(): string
    {
        // TODO: un-hardcode this
        return 'https://apps-apis.google.com/a/feeds/emailsettings/2.0/';
    }


}