<?php
/**
 * Created by PhpStorm.
 * User: rotunomp
 * Date: 6/19/18
 * Time: 12:46 PM
 */

namespace MiamiOH\GoogleApps\Api;


use MiamiOH\RESTng\Util\ResourceProvider;

class EntryResourceProvider extends ResourceProvider
{

    public function registerDefinitions(): void
    {
        $this->addDefinition(array(
            'name' => 'Entry',
            'type' => 'object',
            'properties' => array(
                'type' => array(
                    'type' => 'string',
                ),
                'user' => array(
                    'type' => 'string',
                ),
                'username' => array(
                    'type' => 'string',
                ),
                'domain' => array(
                    'type' => 'string',
                ),
                'firstName' => array(
                    'type' => 'string',
                ),
                'lastName' => array(
                    'type' => 'string',
                ),
                'isAdmin' => array(
                    'type' => 'boolean',
                ),
                'ipWhitelisted' => array(
                    'type' => 'boolean',
                ),
                'suspended' => array(
                    'type' => 'boolean',
                ),
                'mustChangePassword' => array(
                    'type' => 'boolean',
                ),
                'organization' => array(
                    'type' => 'string',
                ),
                'nickname' => array(
                    'type' => 'array',
                    'items' => array(
                        'type' => 'string',
                    ),
                ),
                'groupMembership' => array(
                    'type' => 'array',
                    'items' => array(
                        'type' => 'string',
                    ),
                ),
                'indexed' => array(
                    'type' => 'boolean',
                ),
            )
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'EntryService',
            'class' => \MiamiOH\GoogleApps\Api\EntryService::class,
            'description' => 'Provides user info services.'
        ));
    }

    public function registerResources(): void
    {
        // Line 299 GET
        $this->addResource(array(
            'action' => 'read',
            'description' => 'Get user info.',
            'name' => 'googleApps.entry.v3.read',
            'pattern' => '/googleApps/entry/v3/:id',
            'service' => 'EntryService',
            'method' => 'getEntryInfo',
            'params' => array(
                'id' => array('description' => 'The ID we are searching for'),
            ),
            'options' => array(
                'domain' => array(
                    'required' => false,
                    'default' => '',
                    'description' => 'Domain',
                )
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'A user info entry',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/Entry',
                    )
                ),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array('application' => 'GoogleApps',
                        'module' => 'Entry',
                        'key' => 'read'
                    ),
                ),
            ),
        ));

    }

    public function registerOrmConnections(): void
    {
    }
}