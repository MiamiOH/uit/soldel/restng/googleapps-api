<?php


namespace MiamiOH\GoogleApps\Api;


use MiamiOH\RESTng\Util\ResourceProvider;

class OrgUserResourceProvider extends ResourceProvider
{

    public function registerDefinitions(): void
    {
        $this->addDefinition(array(
            'name' => 'OrgUser',
            'type' => 'object',
            'properties' => array(
                'user' => array(
                    'type' => 'string',
                ),
                'orgUnit' => array(
                    'type' => 'string',
                ),
            )
        ));
    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'OrgUserService',
            'class' => \MiamiOH\GoogleApps\Api\OrgUserService::class,
            'description' => 'Provides orgUser services.'
        ));
    }

    public function registerResources(): void
    {
        // Line 881 POST
        $this->addResource(array(
            'action' => 'create',
            'description' => 'Create new orgUser.',
            'name' => 'googleApps.orgUser.v3.create',
            'pattern' => '/googleApps/orgUser/v3',
            'service' => 'OrgUserService',
            'method' => 'createOrgUser',
            'params' => array(),
            'body' => array(
                'description' => 'A orgUser object',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/OrgUser'
                ),
            ),
            'options' => array(
                'domain' => array(
                    'required' => false,
                    'default' => '',
                    'description' => 'Domain',
                )
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_CREATED => array(
                    'description' => 'A orgUser to be added',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/OrgUser',
                    )
                ),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array('application' => 'GoogleApps',
                        'module' => 'OrgUser',
                        'key' => 'write'
                    ),
                ),
            ),
        ));

    }

    public function registerOrmConnections(): void
    {
    }

}