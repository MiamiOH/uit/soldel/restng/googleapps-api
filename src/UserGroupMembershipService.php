<?php

namespace MiamiOH\GoogleApps\Api;


use GuzzleHttp\Psr7\Request;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Connector\LDAPFactory;
use MiamiOH\RESTng\Util\Response;

class UserGroupMembershipService extends GoogleServiceHelper
{

    /** @var GoogleGroupSourceService */
    private $googleGroupSource;

    public function getMembershipsForUser(): Response
    {
        $response = $this->getResponse();
        $request = $this->getRequest();
        $options = $request->getOptions();
        $uniqueId = $request->getResourceParam('uniqueId');
        $domain = array_key_exists('domain', $options) ? $options['domain'] : $this->getDomain();
        $memberships = [];

        $userGroupMembershipService = $this->getUserGroupMembershipServiceClient();

        try {
            $userGroupMemberShipResponse = $userGroupMembershipService->groups->listGroups(array(
                'userKey' => $uniqueId . '@' . $domain,
            ));
        } catch (\Exception $e) {
            $response->setStatus($e->getCode());
            $response->setPayload([$e->getMessage()]);
            return $response;
        }

        // put the response in a formatted data array
        /** @var $group \Google_Service_Directory_Group*/
        foreach ($userGroupMemberShipResponse->getGroups() as $group) {
            $memberships[] = [
                'groupId' => $group->email,
                'groupName' => $group->name,
                'description' => $group->description,
                'directMember' => true, // since there is no separate "directMember" attribute in the "member" object, I am hardcoding the value "true" for now
                // TODO: Make sure the getGroupSource method is working
                'mu_population_method' => $this->googleGroupSource->getGroupSource($group->email),
            ];
        }

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload( $memberships );

        return $response;
    }

    public function createMembership (): Response
    {
        $request = $this->getRequest();
        $options = $request->getOptions();
        $response = $this->getResponse();
        $requestBody = $request->getData();
        $domain = array_key_exists('domain', $options) ? $options['domain'] : $this->getDomain();

        if (empty($requestBody['role'])) {
            $requestBody['role'] = 'member';
        }

        // Check to see if the body has a valid data model before continuing
        try {
            $this->validateDataModel($requestBody);
        } catch (\Exception $e) {
            $response->setStatus(App::API_BADREQUEST);
            $response->setPayload([$e->getMessage()]);
            return $response;
        }
        $uniqueId = $requestBody['memberId'];
        $groupId = $requestBody['groupId'];
        if ($requestBody['role'] == 'owner') {
            $role = 'OWNER';
        } else if  ($requestBody['role'] == 'manager'){
            $role = 'MANAGER';
        } else {
            $role = 'MEMBER';
        }

        $groupId = $this->fixIdToIncludeDomain($groupId, $domain);

        $groupMembershipService = $this->getUserGroupMembershipServiceClient();

        // The object we are sending
        $newMember = new \Google_Service_Directory_Member();
        $newMember->setEmail($uniqueId . '@' . $domain);
        $newMember->setRole($role);

        $groupMembershipResponse = $groupMembershipService->members->insert($groupId, $newMember);

        $response->setStatus(\MiamiOH\RESTng\App::API_CREATED);

        $gm_out_label = ($role != 'MEMBER') ? 'email' : 'memberId';	// This is for backward compatability with V1

        $var = str_replace('@'.$domain,'',$groupMembershipResponse->email);

        $response->setPayload([
            $gm_out_label => $var,
        ]);

        return $response;
    }


    public function deleteMembership(): Response
    {
        $response = $this->getResponse();
        $request = $this->getRequest();
        $options = $request->getOptions();
        $uniqueId = $request->getResourceParam('uniqueId');
        $groupId = $request->getResourceParam('groupId');
        $domain = array_key_exists('domain', $options) ? $options['domain'] : $this->getDomain();

        $groupMembershipService = $this->getUserGroupMembershipServiceClient();

        $groupId = $this->fixIdToIncludeDomain($groupId, $domain);
        $uniqueId = $this->fixIdToIncludeDomain($uniqueId, $domain);

        $groupMembershipResponse = $groupMembershipService->members->delete($groupId, $uniqueId, []);

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);

        return $response;
    }


    // Returns an authenicated Client for interacting with the Google API
    private function getUserGroupMembershipServiceClient(): \Google_Service_Directory
    {
        $client = $this->getGoogleClient();

        $client->setSubject($this->getAdminUser());

        $userGroupMembershipService = new \Google_Service_Directory($client);
        return $userGroupMembershipService;
    }

    /**
     * @param array $inputData
     * @throws \Exception
     * This function iterates through some data and makes sure the data model
     * is correct. Should a member of the data model be missing, an
     * exception is thrown.
     */
    private function validateDataModel(array $inputData): void
    {
        if (!isset($inputData['memberId'])) {
            throw new \Exception('Body must contain memberId!');
        } elseif (!isset($inputData['groupId'])) {
            throw new \Exception('Body must contain groupId!');

        }

    }

    public function setGoogleGroupSource(GoogleGroupSourceService $googleGroupSource)
    {
        $this->googleGroupSource = $googleGroupSource;
    }
}