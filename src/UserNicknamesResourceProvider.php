<?php


namespace MiamiOH\GoogleApps\Api;


use MiamiOH\RESTng\Util\ResourceProvider;

class UserNicknamesResourceProvider extends ResourceProvider
{

    public function registerDefinitions(): void
    {
        $this->addDefinition(array(
            'name' => 'UserNicknames',
            'type' => 'object',
            'properties' => array(
                'owner' => array(
                    'type' => 'string',
                ),
                'nickname' => array(
                    'type' => 'array',
                    'items' => array(
                        'type' => 'string',
                    ),
                ),
            )
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'UserNicknamesService',
            'class' => \MiamiOH\GoogleApps\Api\UserNicknamesService::class,
            'description' => 'provides user nicknames services.',
        ));

    }

    public function registerResources(): void
    {
        //Line 55 GET
        $this->addResource(array(
            'action' => 'read',
            'description' => 'Get nicknames for a user.',
            'name' => 'googleApps.user.nicknames.v3.read',
            'pattern' => '/googleApps/user/nicknames/v3/:uniqueId',
            'service' => 'UserNicknamesService',
            'method' => 'getUserNicknamesForUser',
            'params' => array(
                'uniqueId' => array('description' => 'The unique ID of the user'),
            ),
            'options' => array(
                'domain' => array(
                    'required' => false,
                    'default' => '',
                    'description' => 'Domain',
                )
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'A User Nicknames information object',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/UserNicknames',
                    )
                ),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array('application' => 'GoogleApps',
                        'module' => 'User',
                        'key' => 'read'
                    ),
                ),
            ),
        ));

    }

    public function registerOrmConnections(): void
    {
    }
}