<?php
/**
 * Created by PhpStorm.
 * User: rotunomp
 * Date: 6/19/18
 * Time: 1:08 PM
 */

namespace MiamiOH\GoogleApps\Api;


use MiamiOH\RESTng\Util\Response;

class EntryService extends GoogleServiceHelper
{

    public function getEntryInfo(): Response
    {

        $response = $this->getResponse();
        $request = $this->getRequest();
        $id = $request->getResourceParam('id');
        $options = $request->getOptions();
        $domain = array_key_exists('domain', $options) ? $options['domain'] : $this->getDomain();
        $entry = [];
        $entryService = null;

        // If a domain was provided as a URL option, pass it into the Client object
        $entryService = $this->getEntryServiceClient($id, $domain);

        // We have to check if the entry we're searching for is a user/alias entry versus a group entry
        try {
            $entryResponse = $entryService->users->get($id . '@' . $domain);

        } catch (\Exception $e) {
            //We didn't get a valid user response.
            if ($e->getCode() == 404 || $e->getCode() == 400) {
                $entryResponse = null; //set this to null so that we can check for a group later
            } else {
                $response->setStatus($e->getCode());
                $response->setPayload([$e->getMessage()]);
                return $response;
            }
        }

        if ($entryResponse) {//if it is a user or a nickname
            //we found a user response.
            //now let's determine if it is a user or an alias (nickname)
            $idIsAlias = false;
            $fullAlias = null;
            //if the aliases contains the sentParameter, we know it is an alias, not a user
            if ($entryResponse->aliases) { //there are aliases to check
                $idWithDomain = $id . '@' . $domain;
                foreach ($entryResponse->aliases as $alias) {
                    if ($alias == $idWithDomain) {
                        $idIsAlias = true;
                    }
                }
            }

            //if the sentParameter was an alias
            if ($idIsAlias) {
                $entry = [
                    'type' => 'alias',
                    'alias' => $idWithDomain,
                    'user' => $entryResponse->primaryEmail,
                    'username' => substr($entryResponse->primaryEmail, 0, strpos($entryResponse->primaryEmail, '@')),
                    'domain' => $domain
                ];

            }
            else { //else, the sentParameter was a user

                $orgValue = $entryResponse->orgUnitPath;
                if(substr($orgValue, 0, 1) == '/'){
                    $orgValue = substr($orgValue, 1);
                };

                // Get user's aliases
                $aliases = [];
                if($entryResponse->aliases){
                    foreach($entryResponse->aliases as $alias){//nicknames
                        $aliases[] = $alias;
                    }
                }

                // Get user's groups
                try {
                    $feed = $entryService->groups->listGroups(array('userKey' => $id . '@' . $domain));
                } catch (\Exception $e) {
                    $response->setStatus($e->getCode());
                    $response->setPayload([$e->getMessage()]);
                    return $response;
                }

                $groupList = [];
                foreach ($feed->groups as $group) {
                    $groupList[] = $group->email;
                }

                $groups = [];
                foreach($groupList as $group){//group memberships
                    $groups[] = $group;
                }

                // Set our response payload
                $entry = [
                    'type' => 'user',
                    'user' => $entryResponse->primaryEmail,
                    'username' => substr($entryResponse->primaryEmail, 0, strpos($entryResponse->primaryEmail, '@')),
                    'domain' => $domain,
                    'firstName' => $entryResponse->name->givenName,
                    'lastName' => $entryResponse->name->familyName,
                    'isAdmin' => $entryResponse->isAdmin ? 'true' : 'false',
                    'agreedToTerms' => $entryResponse->agreedToTerms ? 'true' : 'false',
                    'ipWhiteListed' => $entryResponse->suspended ? 'true' : 'false',
                    'suspended' => $entryResponse->suspended ? 'true' : 'false',
                    'mustChangePassword' => $entryResponse->changePasswordAtNextLogin ? 'true' : 'false',
                    'organization' => $orgValue,
                    'nickname' => $aliases,
                    'groupMembership' => $groups,
                    'indexed' => $entryResponse->includeInGlobalAddressList ? 'true' : 'false'
                ];
            }
        } else { //no user was returned, so let's check if it is a group

                $optparams = array();
                try {

                    try {
                        $group = $entryService->groups->get($id . '@' . $domain, $optparams);
                    } catch (\Exception $e) {
                        $response->setStatus($e->getCode());
                        $response->setPayload([$e->getMessage()]);
                        return $response;
                    }

                } catch (\Exception $e) {
                    $entry = null; //we just want to report that we didn't find anything.
                }
                if ($group) {
                    $entry = [
                        'type' => 'group',
                        'id' => $group->email,
                        'name' => $group->name,
                        'description' => $group->description
                    ];
                }
            }

            $response->setStatus(\MiamiOH\RESTng\App::API_OK);
            $response->setPayload($entry);

            return $response;

        }


    private function getEntryServiceClient(
        string $id = null,
        string $domain = 'gdev.miamioh.edu'): \Google_Service_Directory
    {
        $client = $this->getGoogleClient();

        // Impersonate the user we are getting data for
        $userToImpersonate = $id . '@' . $domain;
//        $client->setSubject($userToImpersonate);
        $client->setSubject($this->getAdminUser());

        $entryService = new \Google_Service_Directory($client);
        return $entryService;
    }


}