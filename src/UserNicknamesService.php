<?php


namespace MiamiOH\GoogleApps\Api;


use MiamiOH\RESTng\Service;
use MiamiOH\RESTng\Util\Response;

class UserNicknamesService extends GoogleServiceHelper
{

    public function getUserNicknamesForUser(): Response
    {
        $response = $this->getResponse();
        $request = $this->getRequest();
        $uniqueId = $request->getResourceParam('uniqueId');
        $options = $request->getOptions();
        $domain = array_key_exists('domain', $options) ? $options['domain'] : $this->getDomain();
        $finalNicknamesArray = [];

        $userNicknamesService = $this->getUserNicknameServiceClient();

        try {
            $userNicknamesResponse = $userNicknamesService->users_aliases->listUsersAliases(
                $uniqueId . '@' . $domain);
        } catch (\Exception $e) {
            $response->setStatus($e->getCode());
            $response->setPayload([$e->getMessage()]);
            return $response;
        }


        // Go through each of the nicknames and add them to an array
        $nicknames = [];
        foreach ($userNicknamesResponse as $nickname) {
            $nicknames[] = (str_replace('@' . $domain, '', $nickname['alias']));
        }

        // Add that array to our final formatted array
        $finalNicknamesArray = [
            'owner' => $uniqueId,
            'nickname' => $nicknames
        ];

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload( $finalNicknamesArray );

        return $response;
    }

    // Returns an authenicated Client for interacting with the Google API
    private function getUserNicknameServiceClient(): \Google_Service_Directory
    {
        $client = $this->getGoogleClient();

        $client->setSubject($this->getAdminUser());

        $userGroupMembershipService = new \Google_Service_Directory($client);
        return $userGroupMembershipService;
    }


}