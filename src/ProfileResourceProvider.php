<?php


namespace MiamiOH\GoogleApps\Api;


use MiamiOH\RESTng\Util\ResourceProvider;

class ProfileResourceProvider extends ResourceProvider
{

    public function registerDefinitions(): void
    {
        $this->addDefinition(array(
            'name' => 'Profile',
            'type' => 'object',
            'properties' => array(
                'username' => array(
                    'type' => 'string',
                ),
                'indexed' => array(
                    'type' => 'boolean',
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => 'ProfileUpdate',
            'type' => 'object',
            'properties' => array(
                'indexed' => array(
                    'type' => 'boolean',
                ),
            )
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'ProfileService',
            'class' => \MiamiOH\GoogleApps\Api\ProfileService::class,
            'description' => 'Provides profile services.',
        ));
    }

    public function registerResources(): void
    {
        // GET
        $this->addResource(array(
            'action' => 'read',
            'description' => 'Get profile for a user.',
            'name' => 'googleApps.profile.v3.read',
            'pattern' => '/googleApps/profile/v3/:uniqueId',
            'service' => 'ProfileService',
            'method' => 'getProfile',
            'params' => array(
                'uniqueId' => array('description' => 'The unique ID of the user'),
            ),
            'options' => array(
                'domain' => array(
                    'required' => false,
                    'default' => '',
                    'description' => 'Domain',
                )
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'A user\'s profile info',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/Profile',
                    )
                ),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array('application' => 'GoogleApps',
                        'module' => 'Profile',
                        'key' => 'read'
                    ),
                ),
            ),
        ));

        // Line 815 PUT
        $this->addResource(array(
            'action' => 'update',
            'name' => 'googleApps.profile.v3.update',
            'description' => 'Updates the given profile object.',
            'pattern' => '/googleApps/profile/v3/:uniqueId',
            'service' => 'ProfileService',
            'method' => 'updateProfileInfo',
            'body' => array(
                'description' => 'A profile object. Partial PUTs are supported.',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/ProfileUpdate'
                ),
            ),
            'params' => array(
                'uniqueId' => array('description' => 'The unique ID of the user'),
            ),
            'options' => array(
                'domain' => array(
                    'required' => false,
                    'default' => '',
                    'description' => 'Domain',
                )
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
//                'authorize' => array(
//                    array('application' => 'GoogleApps',
//                        'module' => 'Profile',
//                        'key' => 'write'
//                    ),
//                ),
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'Confirmation that the update was performed',
                ),
            ),
        ));

    }

    public function registerOrmConnections(): void
    {
    }
}