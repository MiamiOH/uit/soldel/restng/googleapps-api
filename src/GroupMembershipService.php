<?php
/**
 * Created by PhpStorm.
 * User: rotunomp
 * Date: 6/15/18
 * Time: 3:21 PM
 */

namespace MiamiOH\GoogleApps\Api;


use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Exception\BadRequest;
use MiamiOH\RESTng\Util\Response;

class GroupMembershipService extends GoogleServiceHelper
{

    public function getMembershipsForGroup(): Response
    {
        $response = $this->getResponse();
        $request = $this->getRequest();
        $options = $request->getOptions();
        $groupId = $request->getResourceParam('groupId');
        $domain = array_key_exists('domain', $options) ? $options['domain'] : $this->getDomain();

        $groupMembershipService = $this->getGroupMembershipServiceClient();

        /*
         * TODO provide correct behavior for large member lists
         *
         * This resource originally ignored the fact that the Google API has a 200
         * member default. It quietly returns only the first 200 members. The current
         * implementation adds a "fetchAll" option, but there is no indication when
         * the total members exceeds the default and fetchAll would be required.
         *
         * Additionally, the fetchAll option would make fetching large group member
         * lists resource intensive. The next page token should be exposed and the
         * fetching all subsequent pages should be left to the consumer.
         *
         * Unfortunately, that is a larger change I want to take on right now. The
         * current use case is to support manual groups in the Directory Accounts
         * Manager and the fetchAll feature should suffice.
         */
        $memberships = [];
        $nextPageToken = '';
        $fetchAll = $options['fetchAll'] ?? false;

        try {
            do {
                $options = [
                    'maxResults' => 200,
                    'pageToken' => $nextPageToken
                ];

                $groupMemberShipResponse = $groupMembershipService->members->listMembers($groupId . '@' . $domain, $options);

                // Set up the return array
                foreach ($groupMemberShipResponse->members as $member) {
                    $memberships[] = [
                        'memberType' => $member->type,
                        'memberId' => $member->email,
                        'directMember' => 'true',
                        'role' => $member->role,
                    ];
                }

                $nextPageToken = $groupMemberShipResponse->getNextPageToken();
            } while ($fetchAll && $nextPageToken);
        } catch (\Exception $e) {
            $response->setStatus($e->getCode());
            $response->setPayload([$e->getMessage()]);
            return $response;
        }

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload( $memberships );

        return $response;
    }

    public function createMembership (): Response
    {
        $request = $this->getRequest();
        $options = $request->getOptions();
        $response = $this->getResponse();

        if (empty($options['role'])) {
            $options['role'] = 'member';
        }

        // The post contents for this service are contained in the URL options
        try {
            $this->validateDataModel($options);
        } catch (\Exception $e) {
            $response->setStatus(App::API_BADREQUEST);
            $response->setPayload([$e->getMessage()]);
            return $response;
        }

        $memberId = $options['memberId'];
        $groupId = $options['groupId'];
        $domain = array_key_exists('domain', $options) ? $options['domain'] : $this->getDomain();

        if ($options['role'] == 'owner') {
            $role = 'OWNER';
        } else if  ($options['role'] == 'manager'){
            $role = 'MANAGER';
        } else {
            $role = 'MEMBER';
        }

        if(!strpos($memberId,$domain)){
            $memberId = $memberId . '@' . $domain;
        }

        if(!strpos($groupId,$domain)){
            $groupId = $groupId . '@' . $domain;
        }

        $groupMembershipService = $this->getGroupMembershipServiceClient();

        // The new member we're sending
        $newMember = new \Google_Service_Directory_Member();
        $newMember->setEmail($memberId);
        $newMember->setRole($role);
        try {

            $groupMembershipService->members->insert($groupId, $newMember);

        } catch (\Exception $e) {
            if ($e->getCode() == 409) { 	// Member already exists, will update it.

                try {
                    $newMember = $groupMembershipService->members->get($groupId, $memberId);
                    $newMember->setRole($role);
                    $groupMembershipService->members->update($groupId, $memberId, $newMember);
                } catch (\Exception $e) {
                    $response->setStatus($e->getCode());
                    $response->setPayload([$e->getMessage()]);
                    return $response;
                }

            } else if ($e->getCode() == 404) {
                $response->setStatus($e->getCode());
                $response->setPayload(['Could not add member '.$memberId]);
                return $response;
            } else {
                $response->setStatus($e->getCode());
                $response->setPayload([$e->getMessage()]);
                return $response;
            }
        }

        $response->setStatus(\MiamiOH\RESTng\App::API_CREATED);

        return $response;
    }


    public function deleteMembership(): Response
    {
        $request = $this->getRequest();
        $options = $request->getOptions();
        $response = $this->getResponse();

        // The delete contents for this service are contained in the URL options
        $this->validateDataModel($options);

        $memberId = $options['memberId'];
        $groupId = $options['groupId'];
        $domain = array_key_exists('domain', $options) ? $options['domain'] : $this->getDomain();

        if(!strpos($memberId,$domain)){
            $memberId = $memberId . '@' . $domain;
        }

        if(!strpos($groupId,$domain)){
            $groupId = $groupId . '@' . $domain;
        }

        $groupMembershipService = $this->getGroupMembershipServiceClient();

        try {
            $groupMembershipService->members->delete($groupId, $memberId);
        } catch (\Exception $e) {
            $response->setStatus($e->getCode());
            $response->setPayload([$e->getMessage()]);
            return $response;
        }

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);

        return $response;
    }


    // Returns an authenicated Client for interacting with the Google API
    private function getGroupMembershipServiceClient(): \Google_Service_Directory
    {
        $client = $this->getGoogleClient();

        $client->setSubject($this->getAdminUser());

        $userGroupMembershipService = new \Google_Service_Directory($client);
        return $userGroupMembershipService;
    }

    /**
     * @param array $inputData
     * @throws \Exception
     * This function iterates through some data and makes sure the data model
     * is correct. Should a member of the data model be missing, an
     * exception is thrown.
     */
    private function validateDataModel(array $inputData): void
    {
            if (!isset($inputData['memberId'])) {
                throw new BadRequest('URL must contain memberId!');
            } elseif (!isset($inputData['groupId'])) {
                throw new BadRequest('URL must contain groupID!');
            }


    }
}