<?php
/**
 * Created by PhpStorm.
 * User: rotunomp
 * Date: 6/15/18
 * Time: 3:02 PM
 */

namespace MiamiOH\GoogleApps\Api;


use MiamiOH\RESTng\Util\ResourceProvider;

class GroupMembershipResourceProvider extends ResourceProvider
{

    public function registerDefinitions(): void
    {
        $this->addDefinition(array(
            'name' => 'GroupMembership',
            'type' => 'object',
            'properties' => array(
                'memberType' => array(
                    'type' => 'string',
                ),
                'memberId' => array(
                    'type' => 'string',
                ),
                'directMember' => array(
                    'type' => 'boolean',
                ),
                'role' => array(
                    'type' => 'string',
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => 'GroupMembership.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/GroupMembership'
            )
        ));
    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'GroupMembershipService',
            'class' => \MiamiOH\GoogleApps\Api\GroupMembershipService::class,
            'description' => 'Provides group membership services.'
        ));
    }

    public function registerResources(): void
    {
        // Line 543 GET
        $this->addResource(array(
            'action' => 'read',
            'description' => 'Get group memberships for a group.',
            'name' => 'googleApps.groupMembership.v3.read',
            'pattern' => '/googleApps/groupMembership/v3/:groupId',
            'service' => 'GroupMembershipService',
            'method' => 'getMembershipsForGroup',
            'params' => array(
                'groupId' => array('description' => 'The group ID'),
            ),
            'options' => array(
                'domain' => array(
                    'required' => false,
                    'default' => '',
                    'description' => 'Domain',
                ),
                'fetchAll' => array(
                    'required' => false,
                    'default' => '',
                    'description' => 'Indicates if all members should be returned in a single response. (Any value)',
                ),
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'A list of GoogleGroupSourceService Memberships',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/GroupMembership.Collection',
                    )
                ),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array('application' => 'GoogleApps',
                        'module' => 'GroupMembership',
                        'key' => 'read'
                    ),
                ),
            ),
        ));

        // Line 513 DELETE
        $this->addResource(array(
            'action' => 'delete',
            'name' => 'googleApps.groupMembership.v3.delete',
            'description' => 'Deletes the given GoogleGroupSourceService Membership for a user, based on .',
            'pattern' => '/googleApps/groupMembership/v3',
            'service' => 'GroupMembershipService',
            'method' => 'deleteMembership',
            'params' => array(),
            'options' => array(
                'domain' => array(
                    'required' => false,
                    'default' => '',
                    'description' => 'Domain',
                ),
                'groupId' => array(
                    'required' => true,
                    'default' => '',
                    'description' => 'The group to remove from'
                ),
                'memberId' => array(
                    'required' => true,
                    'default' => '',
                    'description' => 'The member to remove'
                ),
                'role' => array(
                    'required' => true,
                    'default' => '',
                    'description' => 'The role of the member being removed'
                ),
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'Confirmation that the delete was performed',
                ),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array('application' => 'GoogleApps',
                        'module' => 'GroupMembership',
                        'key' => 'write'
                    ),
                ),
            ),
        ));

        // Line 474 POST
        $this->addResource(array(
            'action' => 'create',
            'description' => 'Create new group membership.',
            'name' => 'googleApps.groupMembership.v3.create',
            'pattern' => '/googleApps/groupMembership/v3',
            'service' => 'GroupMembershipService',
            'method' => 'createMembership',
            'params' => array(),
            'options' => array(
                'groupId' => array(
                    'required' => true,
                    'description' => 'group ID we want to add someone to'
                ),
                'memberId' => array(
                    'required' => true,
                    'description' => 'member we want to add to our group'
                ),
                'role' => array(
                    'required' => false,
                    'default' => 'MEMBER',
                    'description' => 'role in the group'
                ),
                'domain' => array(
                    'required' => false,
                    'default' => '',
                    'description' => 'Domain',
                )
            ),
            'body' => array(
                'description' => 'A group membership object',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/GroupMembership'
                ),
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_CREATED => array(
                    'description' => 'A group membership to be added',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/GroupMembership',
                    )
                ),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array('application' => 'GoogleApps',
                        'module' => 'GroupMembership',
                        'key' => 'write'
                    ),
                ),
            ),
        ));
    }

    public function registerOrmConnections(): void
    {
    }
}