<?php


namespace MiamiOH\GoogleApps\Api;


use MiamiOH\RESTng\Util\ResourceProvider;

class UserResourceProvider extends ResourceProvider
{

    public function registerDefinitions(): void
    {
        $this->addDefinition(array(
            'name' => 'User',
            'type' => 'object',
            'properties' => array(
                'username' => array(
                    'type' => 'string',
                ),
                'firstname' => array(
                    'type' => 'string',
                ),
                'lastname' => array(
                    'type' => 'string',
                ),
                'suspended' => array(
                    'type' => 'boolean',
                ),
                'suspensionReason' => array(
                    'type' => 'string',
                ),
                'admin' => array(
                    'type' => 'boolean',
                ),
                'mustChangePassword' => array(
                    'type' => 'boolean',
                ),
                'agreedToTerms' => array(
                    'type' => 'boolean',
                ),
            )
        ));

        $this->addDefinition(array(
           'name' => 'UserPostBody',
           'type' => 'object',
           'properties' => array(
               'username' => array(
                   'type' => 'string',
               ),
               'givenName' => array(
                   'type' => 'string',
               ),
               'familyName' => array(
                   'type' => 'string',
               ),
               'password' => array(
                   'type' => 'string',
               ),
           )
        ));
    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'UserService',
            'class' => \MiamiOH\GoogleApps\Api\UserService::class,
            'description' => 'Provides user services.'
        ));
    }

    public function registerResources(): void
    {
        // Line 656 GET
        $this->addResource(array(
            'action' => 'read',
            'description' => 'Get user info.',
            'name' => 'googleApps.user.v3.read',
            'pattern' => '/googleApps/user/v3/:uniqueId',
            'service' => 'UserService',
            'method' => 'getUserInfo',
            'params' => array(
                'uniqueId' => array('description' => 'The unique ID of the user'),
            ),
            'options' => array(
                'domain' => array(
                    'required' => false,
                    'default' => '',
                    'description' => 'Domain',
                )
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'A User\'s info.',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/User',
                    )
                ),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array('application' => 'GoogleApps',
                        'module' => 'User',
                        'key' => 'read'
                    ),
                ),
            ),
        ));

        // Line 408 POST
        $this->addResource(array(
            'action' => 'create',
            'description' => 'Create new user.',
            'name' => 'googleApps.user.v3.create',
            'pattern' => '/googleApps/user/v3',
            'service' => 'UserService',
            'method' => 'createUserInfo',
            'params' => array(),
            'body' => array(
                'description' => 'A user that is to be created',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/UserPostBody'
                ),
            ),
            'options' => array(
                'domain' => array(
                    'required' => false,
                    'default' => '',
                    'description' => 'Domain',
                )
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_CREATED => array(
                    'description' => 'A user to be added',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/User',
                    )
                ),
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array('application' => 'GoogleApps',
                        'module' => 'User',
                        'key' => 'write'
                    ),
                ),
            ),
        ));

        // Line 506 PUT
        $this->addResource(array(
            'action' => 'update',
            'name' => 'googleApps.user.v3.update',
            'description' => 'Updates the given user object. Partial PUTs are supported.',
            'pattern' => '/googleApps/user/v3/:uniqueId',
            'service' => 'UserService',
            'method' => 'updateUser',
            'body' => array(
                'description' => 'A user object',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/User'
                ),
            ),
            'params' => array(
                'uniqueId' => array('description' => 'The unique ID of the user'),
            ),
            'options' => array(
                'domain' => array(
                    'required' => false,
                    'default' => '',
                    'description' => 'Domain',
                )
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array('application' => 'GoogleApps',
                        'module' => 'User',
                        'key' => 'write'
                    ),
                ),
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'Confirmation that the update was performed',
                ),
            ),
        ));

        $this->addResource(array(
            'action' => 'delete',
            'name' => 'googleApps.user.v3.delete',
            'description' => 'Deletes the given user object.',
            'pattern' => '/googleApps/user/v3/:uniqueId',
            'service' => 'UserService',
            'method' => 'deleteUser',
            'params' => array(
                'uniqueId' => array('description' => 'The unique ID of the user'),
            ),
            'options' => array(
                'domain' => array(
                    'required' => false,
                    'default' => '',
                    'description' => 'Domain',
                )
            ),
            'middleware' => array(
                'authenticate' => array('type' => 'token'),
                'authorize' => array(
                    array('application' => 'GoogleApps',
                        'module' => 'User',
                        'key' => 'delete'
                    ),
                ),
            ),
            'responses' => array(
                \MiamiOH\RESTng\App::API_OK => array(
                    'description' => 'Confirmation that the delete was performed',
                ),
            ),
        ));

    }

    public function registerOrmConnections(): void
    {
    }

}