<?php


namespace MiamiOH\GoogleApps\Api;


use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Exception\BadRequest;
use MiamiOH\RESTng\Util\Response;

class UserService extends GoogleServiceHelper
{

    // Gets user info
    public function getUserInfo(): Response
    {
        $response = $this->getResponse();
        $request = $this->getRequest();
        $uniqueId = $request->getResourceParam('uniqueId');
        $options = $request->getOptions();
        $domain = array_key_exists('domain', $options) ? $options['domain'] : $this->getDomain();
        $userInfo = [];

        $userService = $this->getUserClient();

        try {
            $userResponse = $userService->users->get($uniqueId . '@' . $domain);
        } catch (\Exception $e) {
            $response->setStatus($e->getCode());
            $response->setPayload([$e->getMessage()]);
            return $response;
        }

        $userInfo = [
            'username' => str_replace('@' . $domain,'',$userResponse->primaryEmail),
            'firstname' => $userResponse->name->givenName,
            'lastname' => $userResponse->name->familyName,
            'suspended' => $userResponse->suspended,
            'suspensionReason' => $userResponse->suspensionReason,
            'admin' => $userResponse->isAdmin,
            'mustChangePassword' => ($userResponse->changePasswordAtNextLogin ? true : false),
            'agreedToTerms' => ($userResponse->agreedToTerms ? true : false)
        ];


        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload( $userInfo );

        return $response;
    }

    // Creates a new user info object
    public function createUserInfo (): Response
    {
        $request = $this->getRequest();
        $options = $request->getOptions();
        $response = $this->getResponse();
        $requestBody = $request->getData();
        $domain = array_key_exists('domain', $options) ? $options['domain'] : $this->getDomain();

        try {
            $this->validateDataModel($requestBody);
        } catch (\Exception $e) {
            $response->setStatus(App::API_BADREQUEST);
            $response->setPayload([$e->getMessage()]);
            return $response;
        }
        $uniqueId = $requestBody['username'];

        $userService = $this->getUserClient();

        $userEntry = new \Google_Service_Directory_User();
        $userEntry->setPrimaryEmail($uniqueId . '@' . $domain);
        $userName = new \Google_Service_Directory_UserName();
        $userName->setGivenName($requestBody['givenName']);
        $userName->setFamilyName($requestBody['familyName']);
        $userEntry->setName($userName);
        $userEntry->setHashFunction("MD5");
        $userEntry->setPassword(hash("md5", $requestBody['password']));

        try {
            $creationResponse = $userService->users->insert($userEntry);
        } catch (\Exception $e) {
            $response->setStatus($e->getCode());
            $response->setPayload([$e->getMessage()]);
            return $response;
        }

        $response->setStatus(\MiamiOH\RESTng\App::API_CREATED);
        $response->setPayload([
            'username' => str_replace('@'.$domain,'',$creationResponse->primaryEmail),
            'firstname' => $creationResponse->name->givenName,
            'lastname' => $creationResponse->name->familyName,
            'suspended' => $creationResponse->suspended,
            'admin' => $creationResponse->isAdmin,
            'mustChangePassword' => ($creationResponse->changePasswordAtNextLogin ? true : false),
            'agreedToTerms' => ($creationResponse->agreedToTerms ? true : false)
        ]);

        return $response;
    }

    public function updateUser(): Response
    {
        $request = $this->getRequest();
        $options = $request->getOptions();
        $response = $this->getResponse();
        $uniqueId = $request->getResourceParam('uniqueId');
        $data = $request->getData();
        $domain = array_key_exists('domain', $options) ? $options['domain'] : $this->getDomain();

        $userService = $this->getUserClient();

        $updateResponse = null;

        // This block is for updating a user's password
        if(count($data) == 1 && isset($data['password']))
        {

            try {
                $userEntry = $userService->users->get($uniqueId . '@' . $domain);
                $userEntry->setPassword(md5($data['password']));
                $userEntry->setHashFunction("MD5");
                $userService->users->update($uniqueId . '@' . $domain, $userEntry);
            } catch (\Exception $e) {
                $response->setStatus($e->getCode());
                $response->setPayload([$e->getMessage()]);
                return $response;
            }
        }
        // You can't change the password and user info at the same time
        elseif (count($data) > 1 && isset($data['password']))
        {
            $response->setStatus(App::API_BADREQUEST);
            $response->setPayload(['Passwords cannot be set with other data elements.  Please submit only the password value.']);
            return $response;
        }
        // This block changes other user info
        else {
            try {
                $this->updateUserInfo($uniqueId, $domain, $userService, $data);
            } catch (BadRequest $e) {
                $response->setStatus(App::API_BADREQUEST);
                $response->setPayload([$e->getMessage()]);
                return $response;
            } catch (\Exception $e) {
                $response->setStatus($e->getCode());
                $response->setPayload([$e->getMessage()]);
                return $response;
            }
        }

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        return $response;

    }


    public function deleteUser(): Response
    {
        $request = $this->getRequest();
        $options = $request->getOptions();
        $response = $this->getResponse();
        $uniqueId = $request->getResourceParam('uniqueId');
        $domain = array_key_exists('domain', $options) ? $options['domain'] : $this->getDomain();

        $userService = $this->getUserClient();

        try {
            $userService->users->delete($uniqueId . '@' . $domain);
        } catch (BadRequest $e) {
            $response->setStatus(App::API_BADREQUEST);
            $response->setPayload([$e->getMessage()]);
            return $response;
        } catch (\Exception $e) {
            $response->setStatus($e->getCode());
            $response->setPayload([$e->getMessage()]);
            return $response;
        }

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        return $response;

    }



    // Returns an authenicated Client for interacting with the Google API
    private function getUserClient(): \Google_Service_Directory
    {
        $client = $this->getGoogleClient();

        $client->setSubject($this->getAdminUser());

        $userGroupMembershipService = new \Google_Service_Directory($client);
        return $userGroupMembershipService;
    }

    /**
     * @param array $inputData
     * @throws \Exception
     * This function iterates through some data and makes sure the data model
     * is correct. Should a member of the data model be missing, an
     * exception is thrown.
     */
    private function validateDataModel(array $inputData): void
    {
            if (!isset($inputData['username'])) {
                throw new BadRequest('Body must contain username!');
            } elseif (!isset($inputData['givenName'])) {
                throw new BadRequest('Body must contain given name!');
            } elseif (!isset($inputData['familyName'])) {
                throw new BadRequest('Body must contain family name!');
            } elseif (!isset($inputData['password'])) {
                throw new BadRequest('Body must contain password!');
            }

    }

    private function updateUserInfo(
        string $uniqueId,
        string $domain,
        \Google_Service_Directory $userService,
        array $data): \Google_Service_Directory_User
    {
        $userEntry = $userService->users->get($uniqueId.'@'.$domain);

        foreach ($data as $key => $value)
        {
            switch ($key)
            {
                case ('givenName'):
                    $givenName = $value;
                    break;

                case ('familyName'):
                    $familyName = $value;
                    break;

                case ('username'):
                    $userEntry->setPrimaryEmail($value.'@'.$domain);
                    break;

                case ('suspend'):
                    $userEntry->setSuspended( ($value && $value!='false') ? true : false );
                    break;

                default:
                    throw new BadRequest('Unsupported user attribute: ' . $key);
                    break;
            }
        }

        if(!empty($givenName) && !empty($familyName))
        {
            $userName = new \Google_Service_Directory_UserName();
            $userName->setGivenName($givenName);
            $userName->setFamilyName($familyName);
            $userEntry->setName($userName);
        }
        elseif(empty($givenName) && !empty($familyName))
        {
            $userName = new \Google_Service_Directory_UserName();
            $userName->setFamilyName($familyName);
            $userEntry->setName($userName);
        }
        elseif(!empty($givenName) && empty($familyName))
        {
            $userName = new \Google_Service_Directory_UserName();
            $userName->setGivenName($givenName);
            $userEntry->setName($userName);
        }

        $response = $userService->users->update($uniqueId.'@'.$domain, $userEntry);

        return $response;

    }

}