<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/7/18
 * Time: 8:20 AM
 */

return [
    'resources' => [
        'googleApps' => [
            \MiamiOH\GoogleApps\Api\CommonResourceProvider::class,
            \MiamiOH\GoogleApps\Api\CalendarSubscriptionResourceProvider::class,
            \MiamiOH\GoogleApps\Api\UserGroupMembershipResourceProvider::class,
            \MiamiOH\GoogleApps\Api\EntryResourceProvider::class,
            \MiamiOH\GoogleApps\Api\NicknameResourceProvider::class,
            \MiamiOH\GoogleApps\Api\UserResourceProvider::class,
            \MiamiOH\GoogleApps\Api\EmailSettingsResourceProvider::class,
            \MiamiOH\GoogleApps\Api\ProfileResourceProvider::class,
            \MiamiOH\GoogleApps\Api\UserNicknamesResourceProvider::class,
            \MiamiOH\GoogleApps\Api\OrgUserResourceProvider::class,
            \MiamiOH\GoogleApps\Api\GroupResourceProvider::class,
            \MiamiOH\GoogleApps\Api\GroupMembershipResourceProvider::class,
            \MiamiOH\GoogleApps\Api\GroupSettingsResourceProvider::class,
        ],
    ]
];