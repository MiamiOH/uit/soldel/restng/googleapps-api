**Enabling the Google API Service Account:**

https://developers.google.com/api-client-library/php/auth/service-accounts

1. Login to our Google administrative portal as a user that can create projects and service accounts
2. Go to: https://console.cloud.google.com/apis/ to access the Google Cloud Portal
3. Create a new project. If the RTAG user cannot create a project, create a starting project as an admin user, which should enable the RTAG user to do so
4. In the project, select "Enable APIs and services"
5. Enable the appropriate APIs. The two we know that we need are the Calendar API and the Admin SDK
6. Click on the "Credentials" link in the left-hand sidebar
7. Create a new service account key, assign it the project owner role. This will cause a .json file to be downloaded
8. After creation, click on "manage service accounts." On the account that was just created, scroll over to actions -> edit, and check the "Enable G Suite Domain-wide Delegation"
9. Navigate to the main administration page: https://google.com/a/gdev.miamioh.edu
10. In the hamburger menu, Security -> Settings -> Advanced Settings -> Manage API Client Access
11. In the .json file you just dowmloaded there will be a parameter labeled "client_id." Paste this in the "Client Name" box, and provide API scopes as needed

**Using the Email Settings Service:**

https://developers.google.com/gmail/api/guides/migrate-from-email-settings

"
[The Email Settings API] is deprecated and will be turned down in Q4 of 2018. Migrate to the Gmail API as soon as possible to avoid disruptions to your application.
"

Details for interacting with the new Gmail API can be found here:
https://developers.google.com/gmail/api/v1/reference/

**NOTE:** as of 08-17-2018, the EmailSettings Service is not functional. 
